---
layout: single
permalink: /docs/_pages/term1weeks/w1/
title: "WEEK 1 | BOOTCAMP"
excerpt: ""
header:
    image:

last_modified_at: 2019-03-20T20:20:03-05:00
toc: true

---

BOOTCAMP DAYS

---
![](../assets/images/termweeks/w1/01.jpg)
---

Unlike the soldiers I came to this bootcamp totally not equipped in a way but fully driven. I read everything there was to read about this new master before applying and everything that was provided after our enrollment. But still I was not sure what to expect. How to get prepared for that. What plan or strategy to design.
So there I was first official day in the new environment being asked things I’ve been asking myself for so long but still without reaching a certain and fully defined answer. I was so concerned and anxious when I first entered the no201 class but excited at the same time.. It is so intriguing starting a new chapter again, meeting new people, getting new experiences, adding new inputs to whatever you are considered to be.

*bulletnote1: When the conversation started I suddenly forgot my concerns and just felt sure that this time my instinct has somehow helped me take the right decision coming here. It is so comforting hearing 25 people talking for almost 3 hours and not knowing with whom you agree and are related the most. For me it has always been interested how people completely different among them can have so much in common. And being able to work in groups formed on spot was an even more engaging addition to this new beginning. /*

Second day in, we took a deep breath and jumped into the pool called GITLAB. For the time I think I am just trying not to drown.

*bulletpoint2: Learning by doing is one thing but I am so NOT accustomed to this type of documentation and coding is so far from what I can easily understand that I will definitely need a lifejacket sooner or later. Anyway I am happy to explore new unfamiliar paths and I intend to do my best at least to not break the whole system down, although I think this is not even possible to happen./*

I suppose I am quite old fashioned as far as taking notes and documenting my work are concerned. I still like to use a pencil and a notebook and sketch a bit or write down some thoughts that emerge through the lectures or the talks among us.

*bulletpoint3: And the talks by both the directors helped me fill a few pages. And the blur fragments of ideas that were scattered all over my subconscious these previous days starting getting a more stable form./*

---
![](../assets/images/term1weeks/w01_2.jpg)
---

I knew I was concerned on how design could be a political act and how we could move from theory to practice; how can a design has an actual impact on a communities existence. But this was far generalistic. I had this idea of a creating a new system that could provide solutions to troubled communities through co-design processes but how could technology fit into that?

---
![](../assets/images/term1weeks/w01_3.jpg)
---

Are we alone on the pursuit of that goal? Can we do it all by ourselves? Are there enough resources or should we keep it small?

*bulletpoint4: The city safari made clear this. There are so many labs that whatever you may think they can help you make it happen. And that is quite reassuring. This city and especially the Poblenou area has so much to offer as long as you know how to ask for it.*/

---
![](../assets/images/term1weeks/w01_4.jpg)
---

So I just have to find the right question then. Maybe

*bulletpoint5: The experience of trash hunting was even more eye-opening. There is a saying that one man’s trash is another man’s treasure, but to be honest I didn’t really believe that. Until I saw it. The streets of Poblenou are full of treasures every Thursday evening. Material ready to be reused, objects ready to give you some inspiration. I have to admit. I enjoyed it.*/

But the problem was still unsolved. What’s next? How do I move on? As I was trying really hard to concentrate and write down my thoughts I figured out that maybe I don’t need a destination for now. I just need to make the path.


***[I don’t know why this came to me now -I suppose the subconscious works at its own pace; but I’ll write it down anyway for future reference. At some point it was referred that the flow of things is what it is and how we are not supposed to stop it but somehow redefine the path. We need to take the given situation and switch the route a little bit. Not to exclude the primitive instincts but somehow involve them in the procedure in order to work in our advance. Eg. we don’t have to fight against the insatiable desire for new shoes, rather than concentrating on how this fact can help improve the status kuo. For me, making them by recycled materials is not a solution any more. Not making it worse may would have been enough 50 years ago; now we can’t afford that anymore. There should be giving solution to other problems with their actuality.
This also made me thinking that we mostly speak about technology in a way that the western civilization takes for granted. I mean most of the countries belonging to the “first world” are in a great majority technologically literate so the use of it in a different direction may seem like a faraway goal but still manageable. What about the rest of the world though? Would this new condition make the gap even bigger? For example if all western cities manage to apply the “self-sufficient” program, what will happen with the developing countries?]***

I know that my questions may not be answered or maybe they are not even relevant. And for sure I'll get new queries every day.  The upcoming week we are going to have biology classes. I expect to get to know so many new and mind-blowing things about materiality etc. I literally cannot wait!

Well I still do not know who or how I am going to be in 10 years -and probably I won’t know even in 9 years from now- but it’s ok. Baby steps ..

***Note to future self: Don’t worry about the future. 10 years ago you didn’t know you were going to be where you are now.
