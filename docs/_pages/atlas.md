---
layout: single
permalink: /docs/_pages/atlas/
title: "ATLAS of WEAK SIGNALS"
excerpt: ""
header:
    image: /assets/images/atlascover.jpg

last_modified_at: 2019-03-20T20:20:03-05:00
toc: true

feature_row:

    - image_path:
      alt:
      title: "Definitions"
      excerpt: ""
      url: "/docs/_pages/atlas/definitions"
      btn_class: "btn--primary"
      btn_label: "Learn more"

    - image_path:
      alt:
      title: "Development"
      excerpt: ""
      url: "/docs/_pages/atlas/development"
      btn_class: "btn--primary"
      btn_label: "Learn more"

    - image_path:
      alt:
      title: "Submission"
      excerpt: ""
      url: "/docs/_pages/atlas/final"
      btn_class: "btn--primary"
      btn_label: "Learn more"
---

##SHORT DESCRIPTION, || i.e the concept

The Atlas of Weak Signals is a weekly seminar about detecting “weak signals” that set trends and point to certain directions, based on the analysis of the main change factors we can detect in the present, and providing the tools to obtain knowledge based on harvested trends, and data and collectively, built an ontology and concept map and create a navigable repository

---

{% include feature_row %}
