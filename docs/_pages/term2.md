---
layout: single
permalink: /docs/_pages/term2/
title: "TERM 2"
excerpt: ""

last_modified_at: 2019-03-20T20:20:03-05:00
toc: true

feature_row:

    - image_path:
      alt:
      title: "Material Driven Design"
      excerpt: "circular economy of materials"
      url: "/docs/_pages/material"
      btn_class: "btn--primary"
      btn_label: "Learn more"

    - image_path:
      alt:
      title: "Atlas of Weak Signals"
      excerpt: "definitions & development"
      url: "/docs/_pages/atlas"
      btn_class: "btn--primary"
      btn_label: "Learn more"

    - image_path:
      alt:
      title: "FabAcademy"
      excerpt: "making almost anything"
      url: "/docs/_pages/fabacademy"
      btn_class: "btn--primary"
      btn_label: "Learn more"
---

##SHORT DESCRIPTION, || i.e the concept

The second term is formed by 3 paths contributing theoretical knowledge and useful technical skills to the main design studio; the Material Driven Design Studio for the first 6 weeks of the term, the Atlas of Weak Signal Seminar for the next 6 and last but not least the FabAcademy throughout the term.

---

{% include feature_row %}
