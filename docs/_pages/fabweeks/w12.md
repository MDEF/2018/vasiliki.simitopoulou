---
layout: single
permalink: /docs/_pages/fabweeks/w12
title: "WEEK 12 | Applications & Implementations"
excerpt: ""
header:
    image: /assets/images/fabweek12.gif

last_modified_at: 2019-03-20T20:20:03-05:00
toc: true

---

#ASSIGNMENT, || i.e what I should learn to do

"propose a final project that integrates the range of units covered; define the scope of a project; develop a project plan"

---
---

#PROCESS, || i.e what I personally struggled with

My personal project for the master was mostly a quite theoretical thesis, but I am planning to embed principles and skills I gained throughout fabacademy to develop the installation for the final exhibition.

As a brief overall of my MDEF project, I would say that my area of interest is shaped as to examine how trust works, in creating human connections; which parameters affect the way and the subject (or object i.e interface, "contract", etc) we choose to trust. From a personal point of view a reason we face this crisis in trust is because we avoid talking about things that really matter, mostly out of fear; fear that we are going to be judged, we won't fit in. We feel exposed and vulnerable at that very moment. And this is a vicious cycle; the more we avoid this, the more we feel that we are "alone" and no-one understands.

So I decided to "provide" with my project a safe environment where people can express their inner thoughts and feelings and fears; whatever they want to let out. Eventually I ended up researching about the rituals of confession in different contexts, from psychology to religions. I am interested in the topologies and the rituals "traditionally" followed but my main goal though is to create connections through this process; in a broad sense at least. For example I want to keep the anonymity of the "confessant", the typology of the spaces and the catharsis, but at the same time give a twist to the expected process.

For more information and details around the project you can check my [page](https://mdef.gitlab.io/vasiliki.simitopoulou/docs/_pages/proyectopersonal/)

---
---

#OUTCOME, || i.e what I managed to produce

As a layout to develop the project plan, there are a few questions to answer:

*what will it do?*

My plan is to create a "typical" catholic-shaped confession room, where a person can go in, but when sited, there will be a pre-recorded audio playing, where different people will share their stories. In that way the "confessant" is transformed into the confessor but not as a mediator, a priest or a professional psychologist, but as a person that to some extent feels empathy. For me this process can help someone not only relate to other people's feelings and stories but also find his inner, maybe hidden, emotions.
Then after this process is finished, the person would have the opportunity to "confess", this time in a written form (I am not sure yet if it going to have an analog or digital layout). These statements will be projected at the back side of the box-confession room. To keep the anonymity of the confessant this written statement will not happen simultaneously, but every 1 or 2 hours.

![](../fabweeks/assets/images/1201.jpg)

![](../fabweeks/assets/images/1202.jpg)

![](../fabweeks/assets/images/1203.jpg)

*who has done what beforehand?*

An installation that I used as a reference was the [CONFESSIONS](http://candychang.com/work/confessions/), a participatory installation that explores public rituals for catharsis and consolation. Inspired by Japanese Shinto shrine prayer walls and Catholic practices, Chang invited people to write and submit a confession on a wooden plaque in the privacy of confession booths in The Cosmopolitan’s P3 Studio gallery in the heart of the Las Vegas strip. She arranged the anonymous plaques on the gallery walls, painted select responses on 4’ x 4’ canvases, and orchestrated the space with an original ambient score by Oliver Blank. By the end of the exhibit, over 1,500 confessions were displayed in the room.

*what materials and components will be required?*

First of all, I will need a quite big amount of wood as I intent to create a 1:1 replica of a confession room. Then for the audio part, I will need to design a simple speakers system. For that I watched tutorials online, in order to understand how things work but also find what components I might need. I decided to follow this [video](https://www.youtube.com/watch?v=QrgbWWUupZU) as guide, as he is quite organised and methodical and the process seems relatively easy. According to my search I will need a full range speaker (he is using a [4Ω3W](https://goo.gl/kvi8SU) but I am not sure it is the one I will need, as I had not yet designed the exact "box"; most probably I will use a couple similar to this one), a bluetooth power amplifier board, a passive diaphragm and a lithium battery.

*where will they come from?*

Most of the things needed I can find on the streets of Barcelona, thank God for the [map of "Días de Trastos y Muebles"](https://www.google.com/maps/d/u/0/viewer?mid=1l2VAhplHwkWYhNi6WOcDeqnxPoE&ll=41.392682746525104%2C2.1641775000000507&z=13). If I am lucky enough I will get as much wood as needed by collecting doors, cabinets, etc and maybe even parts for the speakers system

*how much will it cost?*

Hopefully, if circular economy goes as expected, the only additional cost will be for glue, screws and cables; a budget of 50E should be more than enough

*what parts and systems will be made?*

Almost all of it I hope. The main structure is just a scale-up from week 7 and now it is "make something big-ER". As for the speakers, if I find the right components it should be pretty easy as well. Basically it is like assembling parts, either fabricated by me or found ready at the streets.

*what processes will be used?*

For the actual exhibition piece, I will use the CNC milling machine for sure to create the "box", the confession room. For some details I may need to use 3D printed objects. I will also use the soldering stations for the audio system.

*what tasks need to be completed?*

-design the "box"; 2D, 3D, details

-use the lasercutter to create a scaled physical modeling

-go city safari for collecting discarded objects in the neighborhoods

-re think the design (if needed)

-get the files ready for the CNC

-cut and sand the parts

-hands on electronics

-finishing details

*what questions need to be answered?*

How I can create a space that people will feel comfortable and motivated at the same time? What details can be added so that the experience is 100% successful?

*what is the schedule?*

I will start with the design this weekend, so that I have some time for physical modeling as well -before moving to fabrication. Then during the week I will go "hunting" for material and according to what I will manage to collect, I will re-evaluate the design. Next step I will spend a day with my beloved raptor, the CNC machine, to get all the pieces cut. And finally I will deal with the electronics' part. The final deadline for these is 7th of June.

-how will it be evaluated?

It is designed as an experience so I suppose that the people trying out will be the ones evaluating it; whether it is efficient, carries its message I want it to. Through their reflections and feedback, after testing it, I will be able to evaluate it indirectly.

---
---

#NOTES, || i.e what I should keep in mind for future reference

-always: measure twice, cut once

#till next week...
