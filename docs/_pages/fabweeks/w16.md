---
layout: single
permalink: /docs/_pages/fabweeks/w16
title: "WEEK 16 | "
excerpt: ""
header:
    image: /assets/images/fabweek16.gif

last_modified_at: 2019-03-20T20:20:03-05:00
toc: true

---

#ASSIGNMENT, || i.e what I should learn to do

""

---
---

#PROCESS, || i.e what I personally struggled with


---
---

#OUTCOME, || i.e what I managed to produce

![](../fabweeks/assets/images/)



---
---

#NOTES, || i.e what I should keep in mind for future reference

-

#till next week...
