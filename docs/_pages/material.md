---
layout: single
permalink: /docs/_pages/material/
title: "MATERIAL DRIVEN DESIGN STUDIO"
excerpt: ""
header:
    image: /assets/images/

last_modified_at: 2019-03-20T20:20:03-05:00
toc: true

feature_row:

    - image_path:
      alt:
      title: "Final Presentation"
      excerpt: ""
      url: "/docs/_pages/materialdriven/materialfinal"
      btn_class: "btn--primary"
      btn_label: "Learn more"

    - image_path:
      alt:
      title: "Extra Assignment"
      excerpt: ""
      url: "/docs/_pages/materialdriven/materialextra"
      btn_class: "btn--primary"
      btn_label: "Learn more"

---

##SHORT DESCRIPTION, || i.e the concept

Materials and fabrication have radically changed in complexity and numbers since the industrial revolution and the environment are on several parameters at a state of collapse, so the situation calls for a new approach to raw materials and manufacturing. This brings us in to a field defined by art, natural science and technology where designers manipulate, grow or develop the material for a product in the same process as designing form and function. This is also called Material Driven Design.

---

{% include feature_row %}
