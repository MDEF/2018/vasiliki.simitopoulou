---
layout: single
permalink: /docs/_pages/term3/
title: "TERM 3"
excerpt: ""

last_modified_at: 2019-03-20T20:20:03-05:00
toc: true

feature_row:

    - image_path:
      alt:
      title: "Design for 2050"
      excerpt: "Collective Imaginations: Stories for the next billion seconds"
      url: "/docs/_pages/design2050"
      btn_class: "btn--primary"
      btn_label: "Learn more"

    - image_path:
      alt:
      title: "Design for the New"
      excerpt: "understanding & reflecting on social practices"
      url: "/docs/_pages/designNEW"
      btn_class: "btn--primary"
      btn_label: "Learn more"

    - image_path:
      alt:
      title: "Emergent Business Models"
      excerpt: "exploring and analysing cases of impact innovations"
      url: "/docs/_pages/emergentbusiness"
      btn_class: "btn--primary"
      btn_label: "Learn more"

    - image_path:
      alt:
      title: "Exhibition Curation"
      excerpt: "one-day workshop with Daniel Charny"
      url: "/docs/_pages/curation"
      btn_class: "btn--primary"
      btn_label: "Learn more"

    - image_path:
      alt:
      title: "FabAcademy"
      excerpt: "making almost anything"
      url: "/docs/_pages/fabacademy"
      btn_class: "btn--primary"
      btn_label: "Learn more"

---

##SHORT DESCRIPTION, || i.e the concept

The third and final term is formed by 3 paths, 3 different short-period modules- contributing theoretical knowledge and useful technical skills to the main design studio; it also involves a one-day workshop by professor Daniel Charny over exhibition curation techniques and the final weeks of Fabacademy.

---


{% include feature_row %}
