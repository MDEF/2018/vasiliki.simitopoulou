---
layout: single
permalink: /home/

header:
  image: /assets/images/homepage.gif

last_modified_at: 2019-03-20T20:20:03-05:00
toc: true

feature_row:

    - image_path: /assets/images/proyecto.png
      alt:
      title: "design studio"
      excerpt: "research; design; develop"
      url: "/vasiliki.simitopoulou/docs/_pages/proyectopersonal/"
      btn_class: "btn--primary"
      btn_label: "Learn more"

    - image_path: /assets/images/fabacademy.png
      alt:
      title: "FabAcademy"
      excerpt: "documentation of FabAcademy; how to make almost anything"
      url: "/vasiliki.simitopoulou/docs/_pages/fabacademy/"
      btn_class: "btn--primary"
      btn_label: "Learn more"

  ---

{% include feature_row %}
