---
layout: single
permalink: /docs/_pages/proyecto/term1/
title: "RESEARCH STUDIO"
excerpt: ""
header:
    image: /assets/images/

last_modified_at: 2019-03-20T20:20:03-05:00
toc: true

---

![](../assets/images/proyectoterm1/00.jpg)

Each one of the 12 weeks of the first term lead me step by step, question to question to the formation of the design studio's project

![](../assets/images/proyectoterm1/01.jpg)

![](../assets/images/proyectoterm1/02.jpg)

---

![](../assets/images/proyectoterm1/03.jpg)

![](../assets/images/proyectoterm1/04.jpg)

![](../assets/images/proyectoterm1/05.jpg)


<iframe width="560" height="315" src="https://www.youtube.com/embed/hVlMqvTUjUU" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

![](../assets/images/proyectoterm1/06.jpg)

---
