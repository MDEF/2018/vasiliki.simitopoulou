---
layout: single
permalink: /docs/_pages/proyecto/term3/
title: "DESIGN STUDIO"
excerpt: ""
header:

last_modified_at: 2019-03-20T20:20:03-05:00
toc: true

feature_row:

 - image_path:
   alt:
   title: "brainstorm"
   excerpt: "intervention proposals"
   url: "/docs/_pages/proyecto/brainstorm3/"
   btn_class: "btn--primary"
   btn_label: "Learn more"

 - image_path:
   alt:
   title: "references"
   excerpt: "state-of-the-art, projects, articles, etc"
   url: "/docs/_pages/proyecto/references/"
   btn_class: "btn--primary"
   btn_label: "Learn more"

---

##BRIEF, || i.e a short conclusion of the second term

From a chaotic atlas of weak signals that was developed collectively during the second term of our studies, I tried to navigate, find my path and narrow it down to a more simple visual directly related to my interests. The atlas may offer countless opportunities but I had to find my entry point(s); the area of my intervention. My mind is like a web browser; 25 open windows with hundreds of tabs each. I don't even know where to start, which signals I can exclude, how to construct the weak signal map of my personal project. So let's take it the other way round. How does the future in which this project is applied look like? How is the society formed, what it means for the people and how can we get there?

Trust...the fundamental of all relations. Is 2050 better? I don't know. But for sure it is different.

##INTRO, || i.e a short preliminary part of the third term

My ulterior goal would be to create a system that is working and have a substance of its own; initiate some actions and connections that can alter and evolve and even scale up according to the needs and sources. I can provide the system with the planned first steps and the platform for them to grow and mutate. Eg. something that started as a "planned" dinner may create a series of autonomous events organised and executed by the participants; an urban game can emerge bonding activities between the members of a newborn community; an anonymous "phone call" may be the first step towards taking the stand and talking about what's important.

---

{% include feature_row %}
