---
layout: single
permalink: /docs/_pages/proyecto/proyectoterms/
title: "DESIGN STUDIO"
excerpt: ""
header:
    image: /assets/images/

last_modified_at: 2019-03-20T20:20:03-05:00
toc: true

feature_row:

  - image_path: /assets/images/proyecto1.png
    alt:
    title: "term1"
    excerpt: "research studio"
    url: "/docs/_pages/proyecto/term1/"
    btn_class: "btn--primary"
    btn_label: "Learn more"

  - image_path: /assets/images/proyecto2.png
    alt:
    title: "term2"
    excerpt: "design studio"
    url: "/docs/_pages/proyecto/term2/"
    btn_class: "btn--primary"
    btn_label: "Learn more"

  - image_path: /assets/images/proyecto3.png
    alt:
    title: "term3"
    excerpt: "development studio"
    url: "/docs/_pages/proyecto/term3/"
    btn_class: "btn--primary"
    btn_label: "Learn more"

---

{% include feature_row %}
