---
layout: single
permalink: /docs/_pages/proyecto/finalsApril/
title: "FINAL TERM PRESENTATION"
excerpt: ""
#header:


last_modified_at: 2019-03-20T20:20:03-05:00
toc: true

---
![](../assets/images/proyectoterm2/april/01.png)
---

##OVERVIEW, || i.e the links with the past research

It all started almost 6 months ago, with me - a recovering architect- trying to burst the pure academic bubble I had created over the past years, to move away from what I already knew and to find new ways and paths of exploration. I always wanted to help make other's lives easier, better in a way, on a practical level, by designing the tools that can realise my ideas and facilitate my goals. After all that's what architecture is to me; mediating relations between people - and them with their environment - by designing the media which can ease the creation of connections and help societies grow bigger & stronger.

At first there was not a solid answer to what I was interested in exploring but a strong intuition coming from a personal aspect; I wanted to understand how trust works & if there is a pattern which we can use to develop it in order to build stronger bonds. Trying to balance between researching and experiencing I recorded my findings for quite a long time in order to map and understand trust. The bottomline of all was a simple realisation; trust is a big, complex, relative, multilayered compilation of emotions, thoughts, acts, experiences and so on. Trust happens when you share stories and problems with people who overtime have done “small things” ". No matter how scientists define and each one of us feel or declare trust, it is widely admitted that trust is built in very small -seemingly insignificant- moments.

Beyond any definition, what made sense for me is that trust is a decision; it occurs the moment you DECIDE to engage and CHOOSE to connect with someone (’s problem). So when do we make that decision? Well..it depends; there are different types of connections resulting different types of trust, according to the number of people involved and also the proximity of their relationship and also different reasons and motives that lead to trust. All these possible connections give a picture of the complexity of the issue but also can give a "starting point" for tracing and rebuilding trust.

I decided to focus on the one-to-one trust as it seems that it is what defines the other relationships as well and see how technology is affecting this type of trust as we live in the 3rd wave of change and our lives and relationships are mediated by technological media. To understand how our lives are going to be influenced even more in the future, we should only mentioned that it is calculated that in the next 40 years, we will spend 520 days watching TV series, 02 years sending messages, 03 years on social media, 06 years watching TV, 08 years online, 10 years in total starring at screens. To fully comprehend the size of the issue, I'll just say that in a typical lifetime we spend 06 years dreaming. It is also really interesting to see how trust is distorted with the use of the internet; for example we consider totally normal to trust someone we don't know at all and sleep in his place or allow him to sleep in ours just because it is registered in an app (airbnb) but we would never trust for the same thing someone we just met.  

So if we are going to stare at screens more than even sleeping in the coming years, at least we can make this time count. Instead of using apps that are addictive and designed to isolate us, what if there was a way to use technology to our advantage and  get the help needed to de-code and build trust?

---
![](../assets/images/proyectoterm2/april/02.png)
---

##STATEMENT, || i.e what are the intentions behind the project

So, I intend to create a trust black-box; an AI tool nurtured by people, fed with stories about human relationships, developed to map and evolved to teach future generations what trust is.

WHY a black-box?

By definition a black box is a complex system or device whose internal workings are hidden or not readily understood, a "history" recorder in an machine. In an aircraft a black box is made up of two seperate pieces of equipment: the flight data recorder (which can record metrics like airspeed, altitude, vertical acceleration and fuel flow, encoding the data tracking more than 700 parameters) and a cockpit voice recorder (which preserves the recent history of the sounds).

Plus there is this huge discussion going on about opening the black boxes, understanding their internal mechanism and reach deep learning. This has important implications for the development of the technology itself, but also an effective application in various fields and may provide meaningful insights into the understanding of unknown territories, such as the human brain mechanism.

---
![](../assets/images/proyectoterm2/april/03.png)
---

##THE TRUST BLACK-BOX, || i.e forming the parameters of the project

So the black-box is used both for its symbolic meaning but also because the mechanism I propose would work somehow like one. But what kind of data does it contain?

WHAT is the trust black-box?

The trust black-box is an algorithm trained by people in order to understand the complex, multilayered issue of trust. By collecting the subjective perceptions of trust, formed by emotions, thoughts, beliefs etc, and finding the links in between them, the algorithm can decrypt trust and create patterns of behaviors related to it.

But what are the parameters that we should consider when trying to understand and map trust? If we acknowledge that trust is formed as a combination of emotions and context and it is both a mental state but also has some metrics -which compose its physiology, then a possible outline of trust could be represented as follows:

---
![](../assets/images/proyectoterm2/april/04.png)
---

##EXECUTION, || i.e the function of the algorithm

Therefore, a black-box of trust should be able to record, analyse and classify all these data. The machine is "adopted" by a pair of people who already have an established relationship or are willing to form one.

HOW does the trust black-box work?

First of all the program should know who are the people "operating" the algorithm; the profiles of the people who are contacting the experiment and share their information with the system. For that reason there is a short description following each one of them and their relationship, forming the context; eg their sex and age, their nationality and educational level and occupation and in addition the type and years of their relationship  

---
![](../assets/images/proyectoterm2/april/05.png)
---

The two parts involved, then, have to feed the system with information by narrating stories and experiences, sharing fears and concerns etc; whatever they feel is relevant to the state of the trust in their relationship. The algorithm by scrapping the data given can extract the information needed to structure this relationship's atlas of trust. Then by "communicating" with all the machines that already did the same and combining the atlas-es, a inter-subjective approach of trust is created.

##PRESENTATION, || i.e the visual representation of the algorithm

To better understand how the algorithm works lets assume that each machine is a ring, each clip on the ring is a variable defining trust and each elastic band a different narration. The narrations are being broken down in keypoints and adjusted to variables and finally forming the "whole story". Then the rings are piled up placing the same variables aligned.

---
![](../assets/images/proyectoterm2/april/06.gif)


---

##IMPLEMENTATION, || i.e the plan for the future

The black-box of trust -once it has enough data- can be a powerful tool if implemented in the educational system. School is NOT just a place to acquire skills, but a community of  learning; it is only fair that both the hemispheres of the brain are given the same attention & the school curriculum is re-designed to help kids live a calmer, wiser, more fulfilled life, embracing their emotions, understanding values and building on trust. Alain de Botton and his School of Life is working towards that direction signifying a possible weak signal as far as the future education is concerned.

A tool kit such as the trust black-box can find a place in this new type of schools, helping kids to learn what trust is and build on it. Jean Piaget's theory of cognitive development suggests that children move through four different stages of mental development. His theory focuses not only on understanding how children acquire knowledge, but also on understanding the nature of intelligence. During the Concrete Operational Stage, developed between the ages of 7 and 11, children begin to think logically about concrete events. They understand the concept of conservation. Their thinking becomes more logical and organized, but still very concrete. Children begin using inductive logic, or reasoning from specific information to a general principle. While children are still very concrete and literal in their thinking at this point in development, they become much more adept at using logic. The egocentrism of the previous stage begins to disappear as kids become better at thinking about how other people might view a situation. While thinking becomes much more logical during the concrete operational state, it can also be very rigid. Kids at this point in development tend to struggle with abstract and hypothetical concepts.
During this stage, children also become less egocentric and begin to think about how other people might think and feel. Kids in the concrete operational stage also begin to understand that their thoughts are unique to them and that not everyone else necessarily shares their thoughts, feelings, and opinions. Thus I would suggest that this is the turning point where the new tools are applied.

---
![](../assets/images/proyectoterm2/april/07.png)
---

##TIMELINE, || i.e the project through these past 6 and future 1.5 months

I put my process so far and my plan for the future in a timeline, you can check [here](https://prezi.com/p/im40eb-dq74g/mdef-april-2019/)
