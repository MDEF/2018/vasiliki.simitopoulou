---
layout: single
permalink: /docs/_pages/proyecto/article/
title: "MDEF ARTICLE"
excerpt: ""
header:
  image: /assets/images/coverarticle.gif

last_modified_at: 2019-03-20T20:20:03-05:00
toc: true

---

##BRIEF, || i.e a short summary of my personal project

October 2018; a nine-month journey was about to begin and I was just another frustrated recovering architect trying to move away from what I already knew. With my diploma on hand, determination and strong intuition, I started this dive into the unknown that is called MDEF; Master in Design for Emergent Futures... What was I to do? -Anything that I wanted. ¿But what did I really want to do?

When I chose Architecture school a few years back I had one thing on my mind; how to do something that really counts; for someone, anyone. I wanted to find a way to facilitate how people communicate and bond into forming communities and how these communities work together, evolve, make things better and easier for their members; And for me that was the aim for my future design actions.

Can an architect design to develop better interpersonal and intergroup adjustments? Well that was the description of my dream job. And that was not about just designing and building infrastructures; I wanted to pursue the social aspect of the architectural design and use all these skills that I attained incidentally during my tenure in the architecture school, eg problem solving, working with teams and speaking in public.

But what future can I imagine, in which I can fulfill that dream? A few months ago, I found it quite hard to pinpoint what was that exactly that I wanted to dive into. Everything seemed so new and fascinating. New technologies, new skills to be obtained. There was a whole new broad horizon ahead and it was captivating. But I needed to pinpoint where do I stand. What is my current place and which are my intentions for the future.

My initial idea came as an intuition, trying to answer the question “who am I and who I want to be”; I am a person interested to understand human relations and try to find ways to make them stronger. Also, on a personal level, people tend to trust me and I tend to bond with people quite easily. But I am also an architect; for better or for worse it is part of my identity. If I had to write in a few words what I (like to) do it would be "create spacial affordances[1]".

So how can I intergrate the personal instinct with what I acquired from my architectural education? Is trust related to space? Can we design to enable or to empower trust? That was the feature that I would like to de-code, understand how it works and re-code in order to use it in a big scale. Combining my hunch and my background load, I decided to work with trust as a tool to bond communities.  

After a series of stimulating workshops, many mind-opening lectures by and talks with interesting innovators, I finally came to good terms with what I was about to do. I started by searching for patterns and “techniques”, conditions and situations were trust is created in order to re-create it in a way. I moved on to investigate how trust is related to major issues of our times, how it is affected and distorted by the evolution of the societies and the media used (technology). And finally I came to propose a design intervention as an answer to many questions that arose along my 9 month journey.

In overall, TRUST is a fundamental issue in any type of relationship, whether it is within oneself, a two-people relationship, or in a strategic alliance, a workspace, a community; we need to confront trust if we expect to create value. But first we need to understand it; break it down and analyse its key aspects;

what is trust?

why is trust important?

do we trust enough?

how does trust work? Is there only one type of trust?

can we measure trust?

what kind of actions are directly related to developing trust?

how is technology affecting trust? can we reverse-engineer it?

![](../assets/images/article/01.gif)

[1] *affordance is what the environment offers the individual. James J. Gibson, coined the term in his 1966 book, The Senses Considered as Perceptual Systems, and it occurs in many of his earlier essays. However, his best-known definition is taken from his seminal 1979 book, The Ecological Approach to Visual Perception: "the affordances of the environment are what it offers the animal, what it provides or furnishes, either for good or ill. The verb to afford is found in the dictionary, the noun affordance is not. I have made it up. I mean by it something that refers to both the environment and the animal in a way that no existing term does. It implies the complementarity of the animal and the environment", Gibson (1979, p. 127)

The original definition in psychology includes all transactions that are possible between an individual and their environment. When the concept was applied to design, it started also referring to only those physical action possibilities of which one is aware. The word is used in a variety of fields: perceptual psychology, cognitive psychology, environmental psychology, industrial design, human–computer interaction, interaction design, user-centered design, communication studies, instructional design, science, technology and society (STS), sports science and artificial intelligence*

---

##CONTEXT, || i.e framing the area of interest

When I started working on the idea of trust I thought that a good launching point to understand trust would be by empowering the strong intuition that led me to take the decision to get involved with it in the first place. I conducted experiments with friends and acquaintances, trying to experience and map what trust means to me, how it is developed between two people, what events, small actions or feelings are involved. After long-discussions, emotionally charged, I had no clue how to describe what was happening; I was trying to note down everything that seemed important and later recall the moments but all I came up with was a series of personal, subjective, stories of trust. How can I define what trust is?

In order to find an answer to that, I decided to move to a more "concrete" source of information. I started reading about trust and how it is described in different disciplines. Definitions differ according to the field of study; eg trust may be referred as a propositional attitude, an abstract relation between an abstract self & an abstract meaning of sentence, or set of behaviors, such as acting in ways that depend on another, a belief in a probability that a person will behave in certain ways. Also feelings of confidence & security that a partner cares may be related to trust; it may be described as an emotional brain state, as the willingness to accept vulnerability based upon positive expectations about another’s behavior. Additionally, trust may be defined as an abstract mental attitude towards a proposition that someone is dependable, a complex neural process that binds diverse representations into a sematic pointer that includes emotions. And these are just a few of the possible responses to the definition of trust.

![](../assets/images/article/02.png)

I am not an expert on sociology, or neuroscience, or psychology, or any other science implicated in the issue of trust -and for sure I cannot become one in just a few months. Still I cannot base a whole project on assumptions and personal experiences. Trying to balance between the two worlds, I decided to look for patterns; for findings that pop-up again and again. Trust is a big, complex, relative, multilayered word; a compilation of emotions, thoughts, acts, experiences and so on. Yet no matter how scientists define and each one of us feel or declare trust, it is widely admitted that trust is built in very small -seemingly insignificant- moments; trust happens when you share stories and problems with people who overtime have done "small things".

Beyond any definition, what made sense for me is that trust is a decision; it occurs the moment you DECIDE to engage and CHOOSE to connect with someone (’s problem). So when do we make that decision? Well..it depends; there are different types of connections resulting in different types of trust, according to the number of people involved and also the proximity of their relationship and also different reasons and motives that lead to trust.

All these possible connections give a picture of the complexity of the issue but also can give a "starting point" for tracing and rebuilding trust. My main concern is that we do not make this decision that often anymore; or we don't really understand what it means or how we make it. So in an effort to break down the overall aspect of trust, I put in a diagram my understanding of trust, starting from the consensus view that everything starts and comes back to the person itself; trust is related to how our brain works, what we feel, what we have experienced etc.

For that reason I place in the middle of the diagram the Person. Then, for me, there are 4 main categories of trust, regarding the parts involved: trusting your own self, another person, a certain group of people and systems. In each "category" there are several reasons of trust and by extension or by analogy mis-trust; to form the diagram I move from aspect to aspect asking "WHY", i.e why we mis-trust or we have lost trust. Some lines in between the aspects are crossed as many of them are either effects or conditions in more than one category. Thus, the outcome is a complex "map" representing the complexity of trust; the trust in (times of) crisis.

![](../assets/images/article/03.png)

Out of all these interconnections I decided to focus on the one-to-one trust as it seems that it is what defines the other relationships as well and it is the one that I can personally relate more. To make it more clear and understand what that type of trust means, I started by experimenting in the real world once again, getting into consideration all possible aspects connected, influencing and being influenced by trust between two individuals. Keeping also in mind the different scientific definitions and acknowledging that trust is formed as a combination of emotions and context and it is both a mental state but also has some metrics -which compose its physiology, I created -to my understanding and perception- the atlas of trust.

![](../assets/images/article/04.png)

My next step was to see what forms trust in our era; which aspects influence what we perceive as trustworthy actions or persons, how we shape our environment to correspond to it and, finally, which are the key elements that mediate our relationships and as a result affect our trust bonds. To realise that, and try to project how trust relations may be in the future, I searched for motifs in the societies of the past that reveal the shift in relationships and put them in a diagram -sort of a timeline. If we use the division by Marsall McLuhan and Alvin Toffler about societies as pre-alphabetic/alphabetic/post-alphabetic and first/second/third wave respectively and try to locate the issue of trust in each one of them, we find ourselves somewhere in between the second and the third wave societies -in limbo- where we try to make amends with the past, and at the same time move forward to the futures.

![](../assets/images/article/05.png)

What's clear to me is that one of the major reasons we are in this position is the change of the mediators; what is in between us, shaping and affecting our relationships. In modern times this medium is translated as technology. But how is trust related to technology? And which kind of technology is that? All human relationships are mediated by technological media, or as Guy Debord states in the "Society of the Spectacle" : *"the spectacle is not just a collection of images, but a social relation among people, mediated by images",...,"the spectacle is assigned with reifying capacities, justifying society as it is".* However, for Debord there is no separation between material "real life" and the false represented one, the spectacle. They are intertwined to such a degree that *"the true is a moment of the false"*, by displaying life, the spectacle negates them by reducing them to mere appearance.

But to what extend? The "Society of the Spectacle" is dated back to 1967, and for source Debord had no idea that internet will be dominating our lives or that our personality will be mixed with an online -social media- persona; yet he made a remarkable job describing the conditions and foreseeing our shift from being to having and -now- merely appearing. To understand though how our lives are going to be influenced even more in the future, we should only mentioned that it is calculated that in the next 40 years, we will spend 520 days watching TV series, 02 years sending messages, 03 years on social media, 06 years watching TV, 08 years online, 10 years in total starring at screens. To fully comprehend the size of the issue, I'll just say that in a typical lifetime we spend 06 years dreaming.

It is also really interesting to see how trust is distorted with the use of the internet; for example we consider totally normal to trust someone we don't know at all and stay in his place or allow him to sleep in ours just because it is registered in an app (airbnb) but we would never trust for the same thing someone we just met. We don't bother getting to know the people living next door and always meet in the elevator, or that person that we share the same bus every single morning going to work; in fact we try to find a way out of any personal connection in these situations (avoiding eye-contact, checking our phone, putting on headphones, etc). On the other hand, at the same time, we seek intimacy, friendship or simply human connection in social media. So in the 3rd wave of changes we are experiencing one new type of -trust- relation both facilitated -in a way- and toughened by the use of tech.

When it comes to trusting people, nowadays, something profound is happening. We become host or guests on Airbnb, we own bitcoin and use apps like Tinder to help find a mate. These are all examples of how technology is creating new mechanisms that are enabling us to trust unknown people, companies and ideas. And yet at the same time, trust in institutions is collapsing. So what's happening here, and who do you trust? A great example is BlaBlaCar. It's a platform that matches drivers and passengers who want to share long-distance journeys together. The average ride taken is 320 kilometers. So it's a good idea to choose your fellow travelers wisely. Social profiles and reviews help people make a choice. You can see if someone's a smoker, you can see what kind of music they like, you can see if they're going to bring their dog along for the ride. But it turns out that the key social identifier is how much you're going to talk in the car.

It's remarkable that this idea works at all, because it's counter to the lesson most of us were taught as a child: never get in a car with a stranger. And yet, BlaBlaCar transports more than four million people every single month. To put that in context, that's more passengers than the Eurostar or JetBlue airlines carry. BlaBlaCar is a beautiful illustration of how technology is enabling millions of people across the world to take a trust leap. A trust leap happens when we take the risk to do something new or different to the way that we've always done it.

Trust is an elusive concept, and yet we depend on it for our lives to function. Therefore the question is, can we reverse-engineer the current reality? My intention is to design for a future condition, and for my design to be relevant, it is urgent to understand the role of the technology in it. Taking into consideration the facts and statistics, it is obvious that our attachment to the screens is going to be a major factor in our lives -even bigger than it is today. But is there a way that can work to our benefit? If we are going to stare at screens more than even sleeping in the coming years, at least can we make this time count? Instead of using apps that are addictive and designed to isolate us, what if there was a way to use technology to our advantage and get the help needed to de-code and build trust?

![](../assets/images/article/06.png)

The answer may lie in between the lines; we refrain from forming personal relations with people we "accidentally" meet but we seize the opportunity -given by social media- to get to know new people. What does "knowing" someone means? How do we relate to people nowadays? Going back to my atlas map I figured out that most of my notes about trust -especially those related to emotions and those we can't really put into words- had one basic condition: we already know a person and step by step we build this strong connection. We share some common background -education, nationality, etc- or we have experienced some things together. In other words we profile people as trustworthy -or not- by instinct -yes!- but also by checking an unwritten list of "credentials".

That made me thinking; we shifted from trusting one another directly because of the numbers -because we do not live in small communities anymore where we know the people around us- and placed our trust in institutions -banks, governments, religions in the past, and to platforms, social media nowadays. And by doing so we went from not knowing the person next door to "getting to know" someone at literally the other end of earth. But does this mean that we are doomed to have connections only through our smartphones? We are not going back to small scale -on the contrary we are moving even upper. But maybe that is an opportunity. If now we trust someone because of the identity he carries, then maybe the solution would be to see beyond that. What is this identity anyway?

![](../assets/images/article/07.gif)

We are commonly identified by our nationality, by the flag on our passport. Though in the not-so-recent years this started not making that much sense. In an "after-the-nation" era what does our passport really mean? Well for me it is a paper that can tell you how easily -or not- depending on you origin country, you can move around the globe. Because it is a fact! People flee; we are nomads again. On the one hand all of us, the first world citizens who in the pursuit of better education, career of simple lifestyle, we move away from our mother countries and become "citizens of the world". On the other hand political turbulence in many regions of the world has increased the number of displaced people fleeing complex emergencies and disasters. Forcibly displaced people include internally displaced people (people who remain in their own countries) as well as refugees (people who cross international borders) and according to UNHCR the number of these people globally is at an all-time high since the end of World War Two. This year’s Global Trends report shows that the world’s refugee population stood at 25.4 million at the end of 2017, 19.9 million of which fall under the mandate of UNHCR.

Refugee crisis; being from Greece I am more than familiar with it. People moving by thousands, often ending up in large camps to find "temporary" settlements. But there is nothing temporary in these situations. People are trapped in this limbo condition for months -if they are lucky- even years. And the camps become slowly but steadily the new cities. We used to say the cities of tomorrow but it is happening; right here, right now. Ephemeral -in theory- cities whose inhabitants have been placed there like pieces in a puzzle. Stand-by cities with no natural origin or evolution, not official recognition, that architecture has not embraced. But a refugee camp is indeed a city, just a barely livable. Running water and the disposal of waste are not the only requisites. Being a refugee is far beyond a state now; it is an identity, a nation of its own, with necessities and rights. Being an architect, I was always concerned about the influence of architecture on people's quality of life; so where do architects stand regarding the refugee crisis? with what kind of tech can we equip people, what infrastructures are needed in the "new cities"?  How can we ensure the integration of populations?

Integration; big word. Social integration is the process during which newcomers or minorities are incorporated into the social structure of the host society. Social integration, together with economic integration and identity integration, are three main dimensions of a newcomers' experiences in the society that is receiving them. A higher extent of social integration contributes to a closer social distance between groups and more consistent values and practices. Bringing together various ethnic groups irrespective of language, caste, creed, etc., without losing one's identity. It gives access to all areas of community life and eliminates segregation. Personally I think this is one of the biggest struggle of our times. Social integration does not mean forced assimilation. Social integration is focused on the need to move toward a safe, stable and just society by mending conditions of social disintegration, social exclusion, social fragmentation, exclusion and polarization, and by expanding and strengthening conditions of social integration towards peaceful social relations of coexistence, collaboration and cohesion.

In my personal perspective social integration can never be achieved if we do not put effort in understanding and relating with what or who is not us. It is relatively easy to find common ground with people we think we are similar, but the challenge is to accept what's different. Identity once again is the core. We are men and women, and non-binary; heterosexual and homosexual, and amphisexual; young and old, and non-ageing; white and black (really is it even a thing still? I thought we left apartheid formally back in the early 90s; but I guess I am wrong); Christians and Muslims, and atheists; Is there a place for all of us? of course yes! We need to understand our body, our mind, what shapes our beliefs, and pay respect to all the others out there that find their identity in something that is not ours. We should remember that the future is not going to be heteropatriarcal, nor white, nor western; thank God -if he exists.

So, going back to the issue of trust; if in the 30 years, the number of people displaced -either willingly or forcibly- is going to be more than double what it is today, and more and more people move to the cities, how can we build trust beyond our apparent differences? According to Alejandro Aravena -a Chilean architect having nominated with the Pritzker Prize and best known for his work with the "do tank" Elemental that aims to tackle problems using a participatory approach- *"it's a fact that people are moving towards cities; and even if counterintuitive, it's good news. Evidence shows that people are better off in cities", ..., "out of the three billion people living in cities today, one billion are under the line of poverty. By 2030, out of the five billion people that will be living in cities, two billion are going to be under the line of poverty", ..., "it is not that people will stop coming to cities. They will come anyhow, but.."*

![](../assets/images/article/08.gif)

In cities, even more overpopulated, diverse, multi-layered, multi-scaled, where will we place trust? How can we get to know the "unknown"? Is there a way we can trigger trust by intervening to the cities' masterplans? Can we "see" beyond what we see? What is the role of technology in that scenario? And what kind of interface can be designed to enable human interaction and ultimately connection? Imagine a world that cared about connecting to more than just the wifi. Don’t get me wrong, I love a strong internet signal as much as the next guy. But meaningful human connections is what all seven billion of us are wired for.

---

##INTERVENTION, || i.e describing the interference

From a chaotic atlas of weak signals that was developed collectively during the second term of our studies, I tried to navigate, find my path and narrow it down to a more simple visual directly related to my interests. The atlas may offer countless opportunities but I had to find my entry point(s); the area of my intervention. My mind is like a web browser; 25 open windows with hundreds of tabs each. I don't even know where to start, which signals I can exclude, how to construct the weak signal map of my personal project. So let's take it the other way round. How does the future in which this project is applied look like? How is the society formed, what it means for the people and how can we got there? Which are the key aspects of today that indicate possible intervention points?

Trust...the fundamental of all relations. Is 2050 better? I don't know. But for sure it is different. First and foremost the technology is evolved and as a result our relation with it and the way it mediates our relations with each other. Second, the number of people displaced either willingly or forcibly skyrocketed and the transformed cities are challenged by the dynamics created by the newly added groups. In my personal perspective these two main axes will drive the changes in the societies not long from now. How will the cities of 2050 look like? Can we re-design spaces in order to facilitate human interaction? (the eternal, everlasting inquiry of the architect; active inclusive public spaces)

So if we picture the societies of 2050 even more diverse than they are today and with no "common ground", experiences, memories, to base and build upon their relations, there should be found a way to enable trust despite this lack. And technology should be an ally. If AI has already reached the levels we assume today it can -or even exceeded these limits- then we should make sure beforehand that we have found a way to build AI on a non-bias stable base; media used should be redesigned in a way to protect human relations and use personal data in more efficient ways and not just to promote consumerism. And for sure this new tech should be accessible to all, equally. And this may seem an opening for new perspectives over what education and occupation is.

But even if we leave aside AI and focus on the "everyday" technology, we can see that the reality as described in the sci-fi series "Black Mirror" is not -or at least it is close to not be- that fictional. The amount of information we receive -sometimes willingly and some other times without our consent- is huge. We check our smartphones approximately 150 times a day, which is translated in once every 6 to 7 minutes while being awake. Through mobile phones, online entertainment services, the Internet, electronic mail, television, radio, newspapers, books, social media etc. people receive every day about 105,000 words or 23 words per second in half a day.

According to a study, conducted by researchers at the University of California-San Diego, under Roger Bon, published by the British “Times of London” and “Telegraph” a few years back, the human brain is loaded daily with 34 GB(!) of information; a sufficient quantity to overload a laptop within a week. How does that affect our brain? And how much information from these daily 34 gigabytes eventually is absorbed by the brain?

According to the researchers, the main effect of information overload is that the human attention to focus is continually hampered and interrupted all too often, which does not help in the process of reflection and deeper thinking. As commented by the American psychiatrist Edward Hallowell, “never in human history, our brains had to work so much information as today. We have now a generation of people who spend many hours in front of a computer monitor or a cell phone and who are so busy in processing the information received from all directions, so they lose the ability to think and feel. Most of this information is superficial. People are sacrificing the depth and feeling and cut off from other people.”

What has really changed in modern times, though, is mainly the nature of the information received, rather than its quantity. Looking at either a computer screen or talking face to face with someone, our brain can absorb the same amount of information. A face to face conversation has its own equivalent of bytes of information, since the brain monitors the expressions of other people, listens to the tones of their voice, etc. According to the same research, if such a face to face conversation causes our brain to store information with rate of 100 megabits per second, then a two-hour conversation with someone will store even more information than the estimated volume of electronic information received within a day in our brain.

So a question naturally comes to mind: If our brain is loaded with so much information every day, will it get full after some years? Paul Reber, professor of psychology at Northwestern University, states that our brain will certainly not get full in our lifetime; we have close to 1 billion neurons in our brain, with each neuron forming connections to other neurons and being capable to hold many memories at a time. So, he estimates that our brain’s memory storage capacity is around 2.5 Petabytes. This is a huge memory capacity that can hold an estimated number of 3 million hours of TV shows. But is this what we need? I mean ok our hard drive may not get full anytime soon, but do we really want to get it stuck with meaningless information? What if in this tornado of incoming data we loose valuable, important information? For sure our ability to process information and focus is disturbed but how can we change the root we have taken?

In a more and more accelerating lifestyle, what can we expect of our brains? How will they function? *“You now have a shorter attention span than a goldfish”*, Time magazine declared in 2015, citing a study by Microsoft Canada. In the words of Microsoft CEO Satya Nadella, “The true scarce commodity is increasingly human attention.” According to the study, the average human attention span is now 8 seconds! So practically we are being bombed out of our minds.. we get all this information but we cannot focus on anything for too long. Are the media designed to "trick" our human mortal brain, and get it addicted to them? Of course they are. They play with the levels of the dopamine exuded. The role of dopamine system has actually been shown to relate directly to information-seeking behaviors in primates. So in a way this is a vicious circle.

Can we de-code how it works and re-create a system with the right affordances to enhance values such trust? Is there a way to use technology to our advantage and decrypt and build trust? It is a fact that our cognitive control abilities that are necessary for the enactment of our goals have not evolved to the same degree as the executive functions required for goal setting, but which co-related aspects can we toggle in order to change this dynamics?

I can locate the relation of some weak signals with the human brain; if I map the connections, I can design corresponding affordances, triggers and mechanisms, that will empower trust and meaningful connections beyond the limitations of familiarity, common (hi)stories, etc. How are our public lives formed? Which key aspects influence our behavior? How are these connected with trust (currently in crisis), public spaces and with each other? I put the keywords in a diagram in order to break down the issue, describe the conditions and build the case study:

![](../assets/images/article/09.jpg)

*To see the full diagram according to my interests and the defined weak signals of the proposed intervention [click here](https://prezi.com/view/uj2yQlfLAInHjGWuy8lL)*

Back in the 1920s when we were envisioning the 2000s we were picturing flying cars, robots that could conquer the world, nano-technology that could cure every decease. But instead, here we were with our smart and smart-er phones, getting more and more distant, isolated, living in our bubbles, "living" social media lives in fear of the outside world. Our offline lives was something that always intrigued me. I belong to that last generation that I can somehow remember a time when internet was not our reality, when being in a public space meant playing hide and seek and not "hide" behind a screen, seeking online connection. But I understand this new era; the way of life has changed, it is not easy to be out there, to stand alone, we feel vulnerable. People around us are not familiar, we do not have shared memories or experiences from our childhood, we do not come from the same background, we live in big and bigger cities, busy, loud and crowded, we do not know the ones living close to us. So in a way it made sense to seek for a type of "contract" that could reassure us that we could build on a relationship -even if this was given by an impersonal platform.

But is that reversible? Can we become again the active "players" in the urban net and connect in a more direct way, overcoming first of all the fear of the unknown? How can we see past our differences and build meaningful relations in order to form stronger communities?

Our current lifestyle and everyday "choices" indicate that feelings like loneliness, anxiety and overwhelm are not going away if we do not change radically the way we perceive our everyday routines. In the diagram there are some words standing out; "safe space", "assumptions", "otherness", "confession", "mindfulness". Using these I intend to create my intervention. What if there was a space where you could let it all out? A personal confession box where you could speak your mind freely? Can we connect to our inner selves and at the same time empower other people to do the same by "sharing" our thoughts? How can this happen without recreating platforms like twitter or youtube?

***If nobody was listening, what would you say?***

---

##REFERENCES, || i.e incorporating the newest ideas and features, state-of-the-art

These are some of the material that acted as inspiration to my intervention; either realised projects, or theoretical research or even sci-fi references. From each one of them I took different elements, from the way they are developed or materialised to their contextual framework or their symbolism:

-[We're Not Really Strangers](https://www.notreallystrangers.com/)

-[So Cards](https://www.socards.org/)

-[Ask Deep Questions](https://www.kickstarter.com/projects/1955220308/ask-deep-questions)

-[Tenemos que vernos mas](https://tenemosquevernosmas.ruavieja.es/en/)

-[CONFESSIONS](http://candychang.com/work/confessions/)

-[The At Hand project by M-cult](https://www.m-cult.org/productions/hand)

-[Childish Gambino festival Installation](https://www.dezeen.com/2018/12/14/childish-gambino-newcastle-wellness-centre-installation/)

-[The Chubby Cloud](https://www.anyahindmarch.com/en-GB/chubby-cloud.html?utm_source=RakutenMarketing&utm_medium=Affiliate&utm_campaign=2116208:Skimlinks.com&utm_content=10&utm_term=UKNetwork&ranMID=37208&ranEAID=TnL5HPStwNw&ranSiteID=TnL5HPStwNw-2GmXcWstHqwibw8gOK7bfg&siteID=TnL5HPStwNw-2GmXcWstHqwibw8gOK7bfg)

-[The School of Life Games & Kits](https://www.theschooloflife.com/shop/games-kits/)

-[Do you Speak Human?](https://space10.io/project/do-you-speak-human/)

-[The Float Project](https://projectfloatcenter.com/)

-[Silence in a World of Noise](https://www.rapal.com/blog/silence-in-a-world-of-noise)

-[Voz Alta by Rafael Lozano-Hemmer](https://vimeo.com/26648281)

-[NY parenthesis installation by zU studio](https://www.designboom.com/design/zu-studio-parenthesis-times-square-05-16-2018/)

-[D Tower](https://v2.nl/lab/projects/d-tower)

-[Pod Phone](https://intoconcept.com/product/pod-phone-booth/)

-[Rohan Chavan's public toilets in Thane, India](https://www.dezeen.com/2016/08/19/rohan-chavan-public-toilets-india-safe-space-women/)

-[Oculus Rift](https://www.engadget.com/2014/07/23/oculus-rift-tuns-x-men-into-a-vr-head-trip/?guccounter=1&guce_referrer=aHR0cHM6Ly93d3cuZ29vZ2xlLmNvbS8&guce_referrer_sig=AQAAAG5IFfNcm7LnWg1swdSA2FnUuVH0JR3Kn0T2kzpRSYB4brPTcr9VBUKBZVkaLFPmKwrRWrX-eLmL7wXa8boD3zw_kBmvPh6d7QyUoU1dsNq8dCpSfhHlX5V4uM6Z2Ac8YH2wmpUJfsSZm1-iZah3JAkgGMjS8QqO8O_3vi8TSyiO#/)

-[Urban Sense](https://www.archive.ece.cmu.edu/~ece549/spring17/team20/website/)

-[Zen Float Tent](https://zenfloatco.com/learn-more-tent)

-[The Otherness Project](https://othernessproject.org/)

-- --[The If Project](https://othernessproject.org/2018/04/24/if/)

-- --[gribskov workshop 2018](https://othernessproject.org/2018/04/23/gribskov-2018/)

-- --[Otherness Dialogues workshop](https://othernessproject.org/2017/12/08/thessaloniki-2017/)

-- --[UrbanArtVentures II](https://othernessproject.org/2017/11/28/urbanartventures-vol-2-in-volos-greece/)

-[Reading Minds](https://www.youtube.com/watch?v=QRz9kXx3YtM)

-["Black Mirror" Fifteen Million Merits](https://www.youtube.com/watch?v=YsEs1R0EVyw)

-[Cerebro](https://en.wikipedia.org/wiki/Cerebro)

-[Professor X](https://www.youtube.com/watch?v=HT-N-uON8r0)

-[The Chatty Maps](http://goodcitylife.org/chattymaps/)

---

##YEAR 2050, || i.e projecting the intervention into the future

May 7th, 2050

A lazy Saturday; but not yet another Saturday. It's my 60th birthday. I picked that date long ago to come back to Barcelona and re-visit the city that I lived for quite a long time around 1 billion seconds ago. This is where it all started after all; the CONF.I.A.N.Ç.A project was my design studio project back in my IAAC years. Memories, accompanied by feelings and thoughts...I took a sip of coffee and allowed myself take a trip down the memory lane.

It was the 2019 when I started this whole project. The world had to face multiple crises at all levels and the -no turning- point was coming closer and closer. Research was focused to how we can extend our abilities; how to mutate ourselves or build "others" to be able do more. But for me the problem was basically the complete opposite. From my personal point of view, we already do a lot, maybe even more than we should. We expose ourselves to technology in a way that is not doing us any good any more. Of course our lives became easier -no argument there- and more "entertaining" in a way, but what did we sacrifice for that? What was important to me was to find the root of all; what we could do in order to change the orbit we had taken. We were at this point where technology was evolving in ways that we could not even imagine or predict, but it was just a tool still; what we do with it was still in our hands.

The city of Barcelona in 2019 was one of the cities with the highest population density in Europe, counting more than 1.7 million residents within the administrative limits and more than 4.6 million in total at the urban area, which means approximately 16,000 people per square km. And this number only represents the official, registered citizens; if we summed up the immigrants that arrived illegally to the city, the thousands of students from abroad and the hundred thousands of tourists visiting Barcelona every day (according to the official report of the ajuntament de barcelona in 2017 nearly 47.3  and another 4.1 million  passengers crossed the city’s airport and port respectively), the total would be much, much higher.

It was a fact that the numbers were not to our advance; and we were not going back.. Barcelona; a city that never sleeps. Locals, students, immigrants, tourists, a diverse population that keeps moving and giving to the city its vivid character. But at the same time it was hard to find a "silent space". Even when the night falls there is a constant "white noise". Lights and sounds forming the spectacle of the city. Yet it was far from metropolis of the world like New York, but it was heading there.

Since then, cities all around the world have been getting louder and louder, and Barcelona is no exception. An important component of urban noise pollution is background music. Streets, schools, cafes and restaurants, every establishment have become so noisy; The noise around us is useful and dangerous. But there is also the "noise" of the cities, which may not influence our hearing but for sure have effects on the psyche and health. Data exchange, information, visuals, big screens and advertisements, lights, and so on. All of them "essential" for a city, outcomes of our choices, of our lifestyles. It is getting more and more difficult to focus or relax, since it seems that wherever you turn your eyes, there is something attracting your attention. Why is it so hard to avoid the temptation of distractions?

Digital forms of media are monopolizing individuals' attention spans, utilizing visual strategies that demand our interactions. Throughout the history of media technology, mediums have become increasingly immersive, presenting more information than ever before. The user interface designs of digital platforms can damage our ability to focus and distribute attention in meaningful ways. As a person who has grown up in a digitized world, I often felt overwhelmed and anxious about the habits I formed with technology. There were countless moments where the only solution to my constant addiction to online information and interaction is physical separation. Especially concerning social media, I could not help but wonder, what do we give up for this instant stimula, a "space" of expression and connection? We share our stories online, we "sell" our privacy, for what?

I was quite aware of both the positive and negative impacts of the digital age, but I was always prompt to seek for potential solutions our society can employ to benefit from the capabilities of digital devices while minimizing distractions and other consequences caused by overuse or maluse. I wanted to find the balance between and get the most of both worlds. The power of internet, allowing us to connect but also collecting valuable data in ways we had never imagined before, was something that should be embedded. But at the same time the negative effects of the use of it should be restrained. And all these, outside our personal bubbles, our "safe places", our mobiles, our houses; on a city scale, at public spaces.

Numerous studies showed that noise -or "noise"- and the lack of privacy were among the biggest concerns, not only for me but for scientist from various disciplines across the world. It has been an issue of discuss and therapy, research material and inspiration for several gadgets for quite a long time. All of them had the same goal: find guidance in order to shield ourselves against "el ruido" -the noise of everyday life. From meditation to psychotherapy people were seeking techniques to find inner peace, silence in a world of noise. Many people have been looking for easy and "flexible" ways to reduce noise and disruptions in their everyday life and to increase happiness in their environment. Mindfulness-based interventions have grown in prominence over the decade.

And then it all began for me. A proposal of creating a "safe space"; a spot in the urban fabric for self-reflection, expression and connection; a contemporary "confession booth", discharged of all the systemic symbolisms (either religious or cultural or whatsoever). My idea was to provide public spaces with a spot of stillness in the craziness of the city's buzz, a silent space without any distractors, where you could let go of anything that bothered you but at the same time connect with others' through the stories they shared.

Of course the first pilot of the program was Barcelona. I placed booths in "crossroads", spots in the city that used to have pedestrian mobility density, such as Passeig de Gracia and Las Ramblas. Locals and tourists have been using them to obtain that moment of relaxation away from the city's high speed life, but the major breakthrough came later. When it draw the attention of the creators of major festivals across the country and world wide it was still in pilot mode, but that was enough to allure them into integrating the concept into their plans. This was one big step towards an industry previously unknown to me.

At the beginning the users could only relate to people that had passed from that specific booth, as the "connection" was created through recorded stories; audios that other users had left in the booth forming a kind of timecapsule. So there was not an actual connection, rather than a trigger to empathise and feel that you are not the only one facing a tough situation. While I was still working on getting more attention to the project as it was, thus more collaboration and funding, I also decided it was time to level up. It was about time to take the pilot to "fly" on a city scale, organised and strategically planned. At that very moment my studio was born with a goal to create more inclusive, meaningful, comforting spaces.

As the years passed by a huge transformation in the design of urban public spaces began. Local governments were more and more starting to rely on data to drive their investment decisions in infrastructure, public transit, and real estate. Driven by the success the prototypes had but also my intention to "fill the city" with the booths, I decided to scale up. But this time not based on what we already knew about the city. I needed new data to increase the efficacy of the project. As the project was envisioning my perspective of mindfulness and a response to modern -at the time- issues of anxiety and disconnection, I contacted organisations already working with bio-data regarding these, and in co-relation data scrapped from social media indicating the existence of similar issues and the demographics, my studio started working on mapping the stress and loneliness of the citizens in the city of Barcelona.

But then I thought, why not give to the citizens the benefit of deciding if and when they want to participate. For that reason I decided to address to the fablab that created the ["Smart Citizen" kit](https://vimeo.com/145620646) -a platform for the generation of social participatory processes in urban areas. By connecting data, people and knowledge, the objective of this platform was to serve as a node for building productive and open indicators, and distributed tools, bringing thereafter to the collective construction of the city for and by its own inhabitants. So what if instead of measuring air pollution, noise pollution or humidity, there was a sensor to detect stress levels?

![](../assets/images/article/010.jpg)

There was already literature over the detection of stress based on multiple biosignals such as electroencephalography, electrocardiography, electromyography, and galvanic skin response. Established by research, a suitable sensor was designed and put in the kit. This step gave us a new vision. Of course the first goal, which was for the advisable spots for placing the booths to be identified, was fulfilled. But this sensor was an opening for an even bigger step. The vision was to make the experience as customised and effective for each user as possible, so what if this sensor could give us some insight towards that direction? What if it was a kind of wearable which the citizens would carry around and give live-feed data? What kind of value could this add to CONF.I.A.N.C.A?

First of it answered a couple of the primitive questions of all times: "who is using the booth and why" and "when does this person know that he really needs it?". The idea was actually really simple; it was a sensor device receiving signals from the user's body and creating valuable data for the user himself (i.e the anxiety/loneliness/etc personal itinerary) and the city planning (i.e location related levels of stress/alienation/etc). So at the same time it was a useful mapping tool but also it could help the user track his habits, get notified if he reaches a critical level and at that point he could make a decision: engage in a different kind of activity in order to soothe the "system" or use the "notification" in order to unlock the CONF.I.A.N.C.A booth. In that way the use would not be "irrational" and the booths would not only be a "safe space", but also secure.

The next step was to reform the booths physically. I was working for quite a long time on the idea of bringing personalisation and customisation, as an aspect of the experience and the interior design of the box. But now with the wearable sensor it could actually be realised. My research had shown that to gain trust and help others open up it is vital to know when it is the right time and what question to ask, as well as the tone of the voice, even the vocal color. On the level of design, initially I kept it as simple and minimalistic as possible -having a black dark empty space- as a reference to silent rooms and in order to make it as commonly accepted as possible. But now the space could be adjusted to anyone's needs or desires; it could become more personal. So my studio's efforts focused on how to extract this kind of data from the sensors and "project" them in order to make the experience more suitable for each one entering the booth.

Another aspect I always wanted to add was the "live" communication. I pictured the booths as portals which could be used to connect people through their stories. The idea was to create an algorithm which could decode the story you were narrating, by extracting keywords and sensing your emotions. Then using the web between the different booths, it could connect you with another spot, where someone was sharing a "similar" story. This way people could find "comfort" in the company of others, people they do not know -or even see- each other at the moment, at a random spot of the city. Like a 21st century hotline, a phone service for personal issues. I always stood by those who stated that we do not really need professional help, just people that will hear us out, empathise and form a connection.

<iframe width="560" height="315" src="https://www.youtube.com/embed/iQS1JrdzEcA" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

"We are not as alone as you think", said professor X to Logan, showing him all the others out there that were exactly as him. I strongly believed in that, I wanted to prove it and here we are; 30 years passed, almost 1 billion seconds... Sometimes it seems like yesterday and some other times like another lifetime. A lot have changed since 2019; cities did, ideas did, I -for sure- did. The initial idea was taken and evolved in ways that I could not picture even in my dreams. The urban silent booths were transformed into powerful points of expression and connections. I wanted to create a safe space where anyone who needs to express his deepest fears, his darkest thoughts, his most radical view would feel welcomed and no one would judge him; a silent garden for those that need to escape the city's craziness and relax from whatever burden, isolated from the media's buzz.

I wanted to see change in the way we connect and communicate. My contribution to that was bringing to the forefront the stories and not the faces; the emotions and not the expressions; the voice and not the looks or the likes. My personal perspective of what it means to develop intimate and meaningful connections was transformed into an intervention, which was developed into a solid project and evolved into an actual "product". CONF.I.A.N.C.A. has changed a lot of people's moments but most importantly created a new context on how we relate to others, to the idea of otherness. *"It is only with the heart that one can see rightly; what is essential is invisible to the eye"*, said the fox to the little prince in Antoine de Saint-Exupéry's novella; and I think the fox got it right.

My intention was re-configure how we perceive and behave in public. I envisioned the public spaces not as leftovers among buildings, but as active, safe, inclusive spaces were everyone would feel catered for who they are. To strike that target I pinpointed the importance of first understanding and healing yourself and putting a better version of you out there; taking a leap of faith, of trust, to create a better system, a healthier environment, a stronger society. They say *"whatever changes you would like to effect in our society has to begin with you"*. The best leaders the world has ever known are the reformers who were accountable and responsible for their own change. But it is not even about being a leader; it is about bring values from within to the world.

Here's to the next billion or even trillion seconds. I hope to see even more evolution to it as the societies grow, the needs and desires change and the spaces are reformed. Beyond a project, CONF.I.A.N.C.A. was a statement, a set of principles that took the form of a booth, but could be re-adjusted according to what the new era will bring. For now, I am happy to see them being part of a lot of cities and evolving into a network of spaces where anyone is welcomed to stay, relax and reflect. 30 years later, we may not read peoples' minds but we speak our own, find a way to stay still and reflect, open our hearts to others. And all these started with a small intervention; a ripple at the pond that was designed back in 2019...

---

##DESIGN ACTION, || i.e documenting the intervention

To decide what design action I should take, I revised all my research and wrote down the basic elements of the intervention. I started from pinpointing the entry points, the gaps in the system(s) I aim to fill, moved to framing my intentions and finally proposed a prototype that answers to my inquiries. Project references helped justifying choices, inspiring images and materialising the concept.

As the first consideration we must acknowledge that in our era we are surrounded by information constantly -both sharing and receiving. We are at that stage where our cities become smarter, the data of daily life is becoming increasingly granular. Sensors and cameras can tell us things like how many people cross a particular street during the morning commute, whether air quality improved over the past year, and whether buses are running on time. And the rise of the smart city has promised to solve fundamental urban problems and make our cities more efficient. But often lost amongst the numbers and hard data is an equally important fact: Cities are populated by humans. What data do we share and how are they being analysed and used? And after all what do we need to know about ourselves, our cities and our living in the cities?

A team of researchers is looking to quantify the more slippery metric of how people feel about their cities, through a series of alternative cartographies. For their most recent project, Daniele Quercia, Luca Maria Aiello, and Rossano Schifanella of Good City Life, created [Chatty Maps](http://goodcitylife.org/chattymaps/). In a lot of ways, Chatty Maps is more art project than science project, but it can give us a picture of what's going on in different cities, in fields previously unexplored. It's an interactive look at the soundscapes of 12 different cities. The idea is that by documenting what people are hearing on the streets, the researchers will be able to glean more insight about how residents perceive and experience their environments. On the same [platform](https://goodcitylife.org/) there are numerous similar projects, eg the "happy maps", the "smelly maps", the "food & health" mapping etc, showing a new -alternative- way to use our own data to map, read and understand our cities.

On the subject of data sharing and use, we cannot turn a blind eye on that we are at the dawn of a generation shaped by the digitization and connection of everything and everyone with the goal of automating much of life, effectively creating time by maximizing the efficiency of everything we do and augmenting our intelligence with knowledge that expedites and optimizes decision-making and everyday routines and processes, we may as well consider that we enter a new phase of the human evolution. But to which direction and at what cost? It is a fact that the most "common" and accessible tech all of us use is the smartphone; don't get me wrong I use it, and I use it a lot; I appreciate the many ways it facilitates my life and keeps me informed and entertained. But I can't help but wonder what are the drawbacks of using it. How addicted are we to our phones and what does this mean for our body, our brain, our lives?

An average person scrolls through 300 feet of mobile content every day; that's equivalent to the size of the Statue of Liberty, or the size of an American football field. We unlock our smartphones almost 110 times a day; that’s almost 10 times every hour, 1 times every 6 minutes we are awake! In [The Distracted Mind](https://mitpress.mit.edu/books/distracted-mind) leading psychologist Larry Rosen, and pioneering neuroscientist Adam Gazzaley, explain why our minds have become addicted to email, text messages, virtual worlds and social media such as Facebook and Twitter; they show how digital distractions affect every aspect of life - from work, safety and communication to our relationships and health. *"The way our brains work means we're all too easily hijacked by 'weapons of mass distraction', but there are countermeasures we can take"*.

![](../assets/images/article/011.jpg)

In general, our ability to distribute our attention is quite limited. We are self-interrupting and not even aware of how often we are diverting our attention from our main task to another task that may be completely unrelated to work. According to Rosen and Gazzaley, *"our cognitive control abilities, that are necessary for the enactment of our goals, is really quite limited: we have a restricted ability to distribute, divide and sustain attention; actively hold detailed information in mind; and concurrently manage or even rapidly switch between competing goals"*. It is proven that the more connected we want to be, the more we are allowing notifications to reach us whenever and unfiltered, the more the distractors affect our routine and thus we mis-perform, but also show high levels of disorders.

Our relationship with technology has spawned a variety of “conditions” that include the phantom pocket vibration syndrome, but also FOMO (fear of missing out), and nomophobia (fear of being out of mobile phone contact), all of which are centered on a need to be connected constantly. Phantom vibrations are an interesting phenomenon. *"A mere ten years ago, if you felt a tingling near your pants pocket you would reach down and scratch the area to relieve the presumed itch. Now the very same neuronal activity promotes a need for us to check our smartphone—sometimes even if we are not carrying one in our pocket at the time—as it is assumed that our phone just vibrated, signaling an incoming alert or notification"* (from the "Distracted Mind"). Overall, symptoms of psychiatric disorders were predicted by some combination of daily technology use and preference for multitasking even after factoring out the impact of anxiety about missing out on technology and technology-related attitudes.

The important waves of technological change are those that fundamentally alter the place of technology in our lives. The Internet is deeply influencing the business and practice of technology. Millions of new people and their information have become interconnected. But what matters is not technology itself, but its relationship to us. If technology plays an important role in our lives -which it does- is there a space for new strategies for regaining and retaining control over where we choose to focus our attention? Can we make the best use of it and help our brain and body at the same time? Over the last two decades, the use of medication prescript to fight anxiety and disorders as ADHD (Attention Deficit Hyperactivity Disorder) in US youth has markedly increased. But for me the solution will not come by fighting the outcomes of the usage of tech, but from preventing it from affecting us to that extend. Sometimes all we need may be to pull the plug...

![](../assets/images/article/012.jpg)

At a time when mental health problems are on the rise, something that improves focus and compassion is certainly something the world needs more of. Over the past decades there have been developed a lot of projects regarding the value of mindfulness. Mindfulness is an ancient eastern practice which is very relevant for our lives today. It is a very simple concept; it means paying attention in a particular way: on purpose, in the present moment, and non-judgementally. It is usually associated with calmness, as if we can learn to be mindful of our thoughts and feelings, to become observers, and we can be more accepting of them. This results in less distressing feelings, and increases our ability to enjoy our lives. Designs that encalm and inform meet two human needs not usually met together. Information technology is more often the enemy of calm; pagers, cellphones, news' services, the World-Wide-Web, email, TV, and even radio or newspapers, billboards, etc bombard us frenetically. Can we really look to technology itself for a solution?

There is a wide variety of ideas developed, from the sensory deprivation tanks -where float tanks are provided for therapeutic sessions and relaxation- to books and mobile applications offering yoga exercises and meditation techniques. *"Something really special is happening with our cultures at a time when we need it most"*, states Megan Jones Bell, the [Headspace’s](https://www.headspace.com) chief science officer. Evidence of their efficacy has been an important driver of their widespread acceptance and proliferation. A whole new "industry" arose.

Although secularised, these mindfulness-based interventions are derived from and influenced by Eastern spiritual traditions, particularly Buddhism, but currently are widely accepted among individuals with perspectives divergent from both secular worldviews and Buddhist narratives. Mindfulness does not conflict with any beliefs or traditions, whether religious, cultural or scientific. It is simply a practical way to notice thoughts, physical sensations, sights, sounds, smells - anything we might not normally notice. Mindfulness can simply be noticing what we don't normally notice, because our heads are too busy in the future or in the past - thinking about what we need to do, or going over what we have done. It regards who we are -or who we want to become- in terms of mind but not identity.

![](../assets/images/article/013.jpg)

Exploring the sparse literature concerning all these issues and trying to find a way to combine my personal interests to create a proposal that could bring changes to the existing system, I conclude to that my focus should be on a personal level. Of course what I would like to influence is in the dynamics of societies but that starts from the individuals. It is our personal needs and desires that are driving us so to achieve any goal we should re-find our center, our point in the world, connect with our feelings and thoughts, understand what is our impact to the world around us and prepare ourselves to be the best version of ourselves.

For that reason I imagined a space were you can freeze time, block out the noise of the city and the buzz of the (social) media and stand still for a moment, facing yourself and letting all go, your thoughts and fears. But self-healing and shielding ourselves against the deficiencies of everyday life was not enough for me. Moreover, I wanted to add the aspect of reciprocity; giving back for what you receive. If we are given the chance to explore and come to good terms with ourselves, then we should reflect on the experience and help with our actions to its development, maybe create a community out of it. After all one of my initial concerns was how our communities are shaped by the means of connection and communication in our times.

That made me thinking what we share in order to communicate who we are and in which ways we choose to connect; the context -what we talk about, when, with whom and why- and the value -how they are being used- of the data themselves but also the importance of sharing in creating meaningful relationships. Is what we share on our social media representative of what we are, of our thoughts and feelings? What would we share if we knew that no one was about to listen, judging us? What things are we afraid to admit even to ourselves? It is not an accidental fact that all the religions embed the concept of confession in order to feel relief and obtain true and honest connection (to God), but also the meditation-related routines invite us to face ourselves, our true colors.

With these parameters in mind, I decided to design the CONF.I.A.N.Ç.A; the acronym stands for the Catalan and Portuguese word meaning trust, and is formed as an abbreviation from the initial components of: CONFess, Initiate, Analyse, Narrate, Confront, Activate. It is referring to an experience, when an individual seeking for stress moderation can enter and participate in an action of contemplation, self-healing and confession. As every ritual, this "personal trip" has different stages starting from becoming aware of the situation you are in and be willing to change it, moving to preparing yourself both physically and mentally, to experiencing the moment and finally reflecting on it.

Trying to place these steps into a tangible diagram of the process, and give to the concept of it a more material aspect, I ended up searching for interventions on a city scale, that could disrupt the everydayness and create new routines. I imagine what a simple box that stands alone in the middle of a big plaza could signify for its surroundings but suggests to the residents, the people that go by this plaza. A box could host this modern-era confession chamber; the silent room where you are invited to relax, reconfigure and express yourself. So the process would be: you come across the box, it raises your attention and you go closer; there are indicators on the outer shell that gives you knowledge or perception of a situation; you move inside the first hut where you are getting ready and then enter the womb, the most "secure", silent and safe space; when the experience is over, you go back to the real world, after going by the reflection point were you can articulate freely.

By this single individual action there would be some data produced -at the moment of reflection; my main focus then was what should happen to them, how they could be used and for which purpose. So I chose to involve them in the process, in a kind of circular economy of data; data produced to be used as catalysts for the experience. In that way not only some would find motivation to express himself, but also it could be possible to find comfort in someone else's "confession" and in a way connect with him through his story.

![](../assets/images/article/014.jpg)

The trial of the project will be a 1:1 scale prototype of the booth placed in the IAAC Atelier. I designed the booth as a 2x2 box with different layers and materialities for each step of the process. For the execution of the project I use a simplified version of the concept. In the plan the 4 different stages of CONF.I.A.N.Ç.A are separated as different parts of the box are destined for each one of them.

On the outside there are posters related the project, with information articulated from the research, as well as speculative narratives regarding the actualised CONF.I.A.N.Ç.A booth. For the preparation part the visitor is asked to leave his personal belongings outside, so that there are no distractors during the experience. Then, before entering, he is advised to take a moment and look at himself at the mirror placed in front of him that reveals only his eyes. To get in the main room, he goes through a hole and lands on a beanbag made of foam. As he lays down, he can hear sounds coming from inside the foam; as he dives deeper into the experience -and the foam- the sounds become more and more clear and finally he get to listen to one -anonymous- confession, a pre-recorded story. As the experience reaches the end the sounds come back and he stands up and moves to the last spot; the spot of reflection. There he is welcomed to write on a big blackboard his answer to the question "if no one was listening what would you confess?"

![](../assets/images/article/015.gif)

The space of the experience has it's own sealing as it needs to be dark and soundproof, as a reference to the principles of sensory deprivation, japanese silent rooms, etc, and amplify the experience itself. There is an extra layer of dark fabric inside the panels both for practical reasons (sound isolation), but also to add an aspect of anticipation and filtering at the entry and exit points. The beanbag is made of soft material, as a big cushion. The sound comes from small speakers placed inside the beanbag -2 at the top and 2 bottom of it, as there are only 2 directions someone can lay down (the size of the interior space equals to a single bed).

![](../assets/images/article/016.png)

---

##SUSTAINABILITY MODEL, || i.e defining future strategy

To follow the initial design action there are several steps already planned, organised per the goal I want to achieve and dissolved into different needs, according to which there are different people, organisations, etc I will address to.

#STEP 1: RAISE AWARENESS // GET ATTENTION TO THE PROJECT

>HOW: by placing mock-up booths in the city of Barcelona

>NEEDS: permission & funding (for materials)

>ADDRESS TO: the Ajuntament of BCN for permission and maybe funding // IAAC & Elisava for funding and collaboration // festivals, eg Sonar, Pride, etc for placing the booths as installations, part of their activities

In this step I am going to re-create low-budget prototypes, copies of the CONF.I.A.N.Ç.A booth designed for the final MDEF exhibition.

Suggested spots in the city are busy crossroads, eg Las Ramblas, Barceloneta, Arc de Triomf, ethnic

---

#STEP 2: MAP THE CITY // PLACE EFFECTIVE BOOTHS

>HOW: by collecting data regarding anxiety levels, creating a map using them and placing the booths at high density spots of the city

>NEEDS: data, know-how, collaborations & funding

>ADDRESS TO: people, studios, universities, organisations, companies, etc collecting, analysing and visualising data, eg Carlo Ratti, Domestic Data Streamers (short term) // neuroscientists, psychologists, sociologists researching on stress indicators (long term) // IAAC & Elisava for funding and collaboration

In this step I am going to create maps of the city of Barcelona according to the stress levels of the citizens and locate the high density points -where stress levels peak- to locate the CONF.I.A.N.Ç.A booths. For that I will need special knowledge and to collect data using different sources:

-for possible collaborations and access to the learning process / eg [MIT senseable city](http://senseable.mit.edu/) & Carlo Ratti

-for collecting existing data, from

--sensors that are already used / eg sports' clubs, NIKE, APPLE, SAMSUNG

--social media posts with relevant hastags / eg Twitter

-for creating a system to collect my own data

--through a performance or installation / eg [Domestic Data Streamers](http://domesticstreamers.com/case-studies/)

--through an open-source kit with sensors detecting hormones, heart beat, etc / eg [Smart Citizen kit](https://vimeo.com/145620646)

At this point I will search also for programs funded by the EU about cities, new technologies, etc. If there is something suitable, I will address to universities, in order to form a research team and work on the project.

---

#STEP 3: BOOTHS 2.0

>HOW: by re-designing the booths / creating the real-time communication portals / adding "human factors"

>NEEDS: know-how, collaborations & funding

>ADDRESS TO: people, studios, universities, organisations, companies, etc working on telecommunications, networks // programmers and developers // neuroscientists, psychologists, sociologists researching on trust issues, anxiety, loneliness, etc // IAAC & Elisava for funding and collaboration

In this step I am going to reform the proposed booths by adding different factors. First I would like to see a network of booths connected with each other where at the moment of confession, when data are created, an algorithm will be generated that can break down the narration (keywords and emotions), set a signal and -if it finds a matching signal- open a "call" where the two confessants can communicate real time. Then I will find a way to involve more personal approach to the concept. For example I would like to add sensors that could detect which is the most suitable time for a question (out of a set of pre-recorded questions) to pop-up to trigger the reflection of the user.

---

#STEP 4: BOOTHS 3.0

>HOW: by creating an app related to the project and re-designing the booths

>NEEDS: know-how, collaborations & funding

>ADDRESS TO: people, studios, universities, organisations, companies, etc working on telecommunications, networks // programmers and developers // neuroscientists, psychologists, sociologists researching on trust issues, anxiety, loneliness, etc // IAAC & Elisava for funding and collaboration

In this step I am going to add another factor: the customisation. For that purpose I will develop a mobile application that will be linked to the booths, which can be used at any time, at the comfort of our houses or offices. This app will work as a personal booth that stores data related to the narratives we share. When using it we will be asked also about preferences regarding our spaces, the sounds, the colours, or even the smells. With this app we will also get access to the actual booths placed throughout the city. In that way when we use the app to unlock the booth, the interior will be customised in order to make our experience more effective.

---

#STEP 5: POP-UP BOOTHS & REAL-TIME DATA

>HOW: by using the kits and apps created in previous steps

>NEEDS: know-how, collaborations & funding

>ADDRESS TO: people, studios, universities, organisations, companies, etc working on telecommunications, networks // programmers and developers // IAAC & Elisava for funding and collaboration

In this step I am going to create live-streaming maps by detecting the signals from the apps and kits, showing which area of the city is high density at which moment of the day and thus CONF.I.A.N.Ç.A is the most needed. In that way the booths can appear and disappear according to the needs. For that purpose the booths should be re-designed, probably with inflated materials and a structure that allows easy and fast construction and disconstruction.

---

##FINAL REFLECTION, || i.e the learning process, results & experience of MDEF

*October 2018; a nine-month journey was about to begin and I was just another frustrated recovering architect trying to move away from what I already knew. With my diploma on hand, determination and strong intuition, I started this dive into the unknown that is called MDEF; Master in Design for Emergent Futures... What was I to do? -Anything that I wanted. ¿But what did I really want to do?*

June 2019; the nine-month journey is about to end. I am still a recovering architect but these months changed me a lot. First and foremost I spend almost every single day with amazing designers, future leaders at whatever they choose to do, my class, the first ever generation of MDEF. We are so different but somehow we share the same principles, motives, concerns for the future. I am for ever grateful I had the opportunity to walk through this experience with each one of them.

Then if I look back to all the learning I got in just 9 months...Where do I start? From the mind-blowing and accelerating 12 weeks of the first term when we were exposed to all these different aspects, to the second and third term with all the mini-workshops adding value and knowledge to help us build up our personal projects. And the Fab Academy...something I would never choose for myself but now I can really appreciate the fact it was part of our program. -I learned the basic in coding; for me that is like a huge step.

From biology and material subjects at the week no2 of Biology Zero and the Material Driven Design Studio, to hands-on exercises with machines at the Design for the Real Digital World week and electronics at the From Bits to Atoms week, to theoretical research and analysis at An Introduction to Futures, to storytelling techniques at the Engaging Narratives week, to many more, I must admit that we covered a wide spectrum of knowledge.

In overall, I may not have had the same enthusiasm or interest every single day, and for sure there where moments of frustration or overwhelm, but I think that every one of the guests or host lecturers help me and my project evolve to a great extend. If I had to choose three out of all of the moments, I would definitely pick:

-the Weak Signal seminar, where we were given the insights to the current trends, how to detect them and how to be able to intervene on the right point in order to change the direction the system has taken. It was not only stimulating to get all this information but interesting to see how almost anything can relate to everything, the systemic crises are parts of the same web, and intriguing to find our spot in this atlas of colliding vectors

-the Design for 2050 workshop, where in just 12 hours we create our version of sci-fi series. From picking existing metropolis of the world, to creating imaginary characters and stories to connect them, our collective work was fascinating. The way that everything could make sense in 1 billion seconds from now gave a new perspective to what we are called to do as designers. To shape a fictional story of a future help me realise that anything is possible as long as I believe in it.

-the Design Studio; my personal project. What started as a hunch, as a personal concern, evolved into an intervention soon to be realised. I had my ups and downs, my moments of clarity and the darkest hours all throughout the process, but somehow everything makes sense now. All these notes, from movies to lectures, are now finally formed into a project that not only reflect what I learned but also who I became.

9 months ago a journey to the unknown started, an Odyssey. I brought to the table my design skills, curiosity and good vibes and expected to get stimulating talks and engaging exercises. I received even more. Through this course we gained the foundations to become the designers, futurists, specialists with a unique description -as unique as each one of us.

270 days ago I was asked where I would want to be in 10 years. I did not know the answer. I am still not sure; after all I am the kind of person that do not want to plan far ahead because life is unpredictable and it is better if I am prepared for the uncertainty rather than trying to certify an erratic future. This master showed me once again how much there is out there yet to be discovered, to understand and integrate, which can add or take out things of our paths.

23 million seconds passed and I may still not know the answer to the "where I am going to be in 10 years" question but what I know for sure is that I am happy and content at the end of the MDEF experience and the beginning of the next one -whatever this is. I would like to see my personal project evolving as I pictured it and even better. But for now I am grateful for everything I learned these past few months, the people I met and worked with and I am looking forward to the futureS!

Here's to the next 30 billion seconds...

![](../assets/images/article/017.gif)





















---
