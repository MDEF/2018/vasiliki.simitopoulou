---
layout: single
permalink: /docs/_pages/proyecto/references/
title: "REFERENCES"
excerpt: ""
header:

---

##LITERATURE, || i.e papers, articles, books

-[The breakdown of cooperation in iterative real-time trust dilemmas](https://link.springer.com/article/10.1007%2Fs10683-006-7049-4)

-[Trust Among Strangers](https://papers.ssrn.com/sol3/papers.cfm?abstract_id=702662)

-[Three-Player Trust Game with Insider Communication](https://digitalcommons.chapman.edu/esi_working_papers/55/)

-[Playing both roles in the trust game](http://community.middlebury.edu/~jcarpent/papers/PBR%20JEBO%2051(2)%202003.pdf)

-[Equilibrium play and adaptive learning in a three-person centipede game](https://www.sciencedirect.com/science/article/pii/S0899825603000095?via%3Dihub)

-[An experimental study of the repeated trust game with incomplete information](https://www.sciencedirect.com/science/article/pii/S0167268101002165?via%3Dihub)

-[How to identify trust and reciprocity](https://www.sciencedirect.com/science/article/pii/S0899825603001192?via%3Dihub)

-[Trust, Reciprocity, and Social History](https://www.sciencedirect.com/science/article/pii/S0899825685710275?via%3Dihub)

-[Trust and risk revisited](https://www.sciencedirect.com/science/article/abs/pii/S0167487016301209?via%3Dihub)

-[The Enabling Power of Trust](https://sloanreview.mit.edu/article/the-enabling-power-of-trust/)

-[Trust Games: A meta-Analysis](http://noeldjohnson.net/noeldjohnson.net/Research_files/Trust%20Games.pdf)

-[We're Not Really Strangers](https://www.notreallystrangers.com/)

-[So Cards](https://www.socards.org/)

-[Ask Deep Questions](https://www.kickstarter.com/projects/1955220308/ask-deep-questions)

-[Tenemos que vernos mas](https://tenemosquevernosmas.ruavieja.es/en/)

-[CONFESSIONS](http://candychang.com/work/confessions/)

-[The At Hand project by M-cult](https://www.m-cult.org/productions/hand)

-[Childish Gambino festival Installation](https://www.dezeen.com/2018/12/14/childish-gambino-newcastle-wellness-centre-installation/)

-[The Chubby Cloud](https://www.anyahindmarch.com/en-GB/chubby-cloud.html?utm_source=RakutenMarketing&utm_medium=Affiliate&utm_campaign=2116208:Skimlinks.com&utm_content=10&utm_term=UKNetwork&ranMID=37208&ranEAID=TnL5HPStwNw&ranSiteID=TnL5HPStwNw-2GmXcWstHqwibw8gOK7bfg&siteID=TnL5HPStwNw-2GmXcWstHqwibw8gOK7bfg)

-[The School of Life Games & Kits](https://www.theschooloflife.com/shop/games-kits/)

-[Do you Speak Human?](https://space10.io/project/do-you-speak-human/)

-[The Float Project](https://projectfloatcenter.com/)

-[Silence in a World of Noise](https://www.rapal.com/blog/silence-in-a-world-of-noise)

-[Voz Alta by Rafael Lozano-Hemmer](https://vimeo.com/26648281)

-[NY parenthesis installation by zU studio](https://www.designboom.com/design/zu-studio-parenthesis-times-square-05-16-2018/)

-[D Tower](https://v2.nl/lab/projects/d-tower)

-[Pod Phone](https://intoconcept.com/product/pod-phone-booth/)

-[Rohan Chavan's public toilets in Thane, India](https://www.dezeen.com/2016/08/19/rohan-chavan-public-toilets-india-safe-space-women/)

-[Oculus Rift](https://www.engadget.com/2014/07/23/oculus-rift-tuns-x-men-into-a-vr-head-trip/?guccounter=1&guce_referrer=aHR0cHM6Ly93d3cuZ29vZ2xlLmNvbS8&guce_referrer_sig=AQAAAG5IFfNcm7LnWg1swdSA2FnUuVH0JR3Kn0T2kzpRSYB4brPTcr9VBUKBZVkaLFPmKwrRWrX-eLmL7wXa8boD3zw_kBmvPh6d7QyUoU1dsNq8dCpSfhHlX5V4uM6Z2Ac8YH2wmpUJfsSZm1-iZah3JAkgGMjS8QqO8O_3vi8TSyiO#/)

-[Urban Sense](https://www.archive.ece.cmu.edu/~ece549/spring17/team20/website/)

-[Zen Float Tent](https://zenfloatco.com/learn-more-tent)

-[The Otherness Project](https://othernessproject.org/)

-- --[The If Project](https://othernessproject.org/2018/04/24/if/)

-- --[gribskov workshop 2018](https://othernessproject.org/2018/04/23/gribskov-2018/)

-- --[Otherness Dialogues workshop](https://othernessproject.org/2017/12/08/thessaloniki-2017/)

-- --[UrbanArtVentures II](https://othernessproject.org/2017/11/28/urbanartventures-vol-2-in-volos-greece/)

-[Reading Minds](https://www.youtube.com/watch?v=QRz9kXx3YtM)

-["Black Mirror" Fifteen Million Merits](https://www.youtube.com/watch?v=YsEs1R0EVyw)

-[Cerebro](https://en.wikipedia.org/wiki/Cerebro)

-[Professor X](https://www.youtube.com/watch?v=HT-N-uON8r0)

-[The Chatty Maps](http://goodcitylife.org/chattymaps/)

---

##PROJECT, || i.e relevant state-of-the-art

-[the urban conga](http://www.theurbanconga.com/projects)

-[Just Add People](http://justaddpeople.co/en)

-[Monopoly in the Park](http://www.monopolyinthepark.com/)

-[The Evolution of Trust](https://ncase.me/trust/)

-
