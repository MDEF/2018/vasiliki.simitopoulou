#08. Be your own system

Week 8 and now it is time to face reality (and fears): we have to confront our personal project idea and experiment with them. But first some one-day workshops.

#Day1 : Artefact#

**artifact: /ˈɑːtɪfakt/ an object made by a human being, typically one of cultural or historical interest: something observed in a scientific investigation or experiment that is not naturally present but occurs at a result of the preparative or investigative procedure**


First day of the week and we can already tell what’s coming. We are exposed to this whole new world of object-project that gives us an entire new perspective on our own ideas. *We usually design something, even produce if we are lucky enough we even face the production phase but after that nothing; this is the end of our common life. What if after we create something we live with it?*
So let’s get an everyday object and turn it into an artifact. How will the environment in which it will be imported get affected? How will the space or the behaviour of people using it transform?

\\\ This is an interesting object; it is made out of glass and you can notice all these shapes inside. But does it have any use? Maybe not; maybe it is just a decorative piece that serves no other purpose.

Obj

It got my attention for sure but I am not quite sure why. Let’s examine it more…

Obj with light

Ok if you put it under a specific light source you get an interesting result. The different shapes are being projected on the surfaces surrounding the object creating strange effects.

Obj boomerang

If you put it in motion you can witness a strange effect which to my eyes looks like water; and it is helping me relax. Water is always soothing in a way. So what if this object could work as a “hypnosis” crystal? When you feel stressed, wherever you are, you can have it with you. You only need to find an almost dark space, get a spot light source and play with the artifact. ///

Obj video

**that short experiment helped me realise some things regarding my general personal project and made me think about space related to senses, primarily sight -or the lack of it, the importance of light and sound and last but not least how some shapes can trigger memories and help us “recreate” moments or feelings**

#Day2 : Green screen#

Ok so today the schedule says “wearing the digital” and we know we should wear bright -preferably green- clothes and play with the app called Chroma Key. I couldn’t help but remember this iconic moment of Queen Elizabeth’s birthday almost 2 years ago -when she wore this statement green outfit and the internet went crazy!
*one of my favorites*

GOD SAVE THE QUEEN

*source: https://www.boredpanda.com/queen-elizabeth-green-screen-outfit-funny-photoshop-battle/?utm_source=google&utm_medium=organic&utm_campaign=organic*

\\\ Using green screen is really common in the film industry in order to add special effects at the post production phase, but what happens when the projection sheet is wearable? If we can give any aesthetic, materiality or media, then what would we wear? And most importantly, why would we wear that?

Green screen photos

It is interesting how our own body can be a canvas and we can use different projections to express yourself. For me, there is a close relation between how we feel and how we dress. I don’t mean it in a fashion statement way, but I think that the clothes we wear send some signals to our pees and strangers, projecting the self image of us that we want to display at that exact moment. An array of psychological surveys have revealed the true impact of clothing choices on the way in which we perceive and judge each other, with experiments showing some surprising results. ///

**yet my inquiry goes beyond the fact that we give certain signals with our wardrobe; I wonder how this signal exchange works. Does the subconscious do all the hard work? Do we make conscious decisions on the subject or is it a mood-driven choice? And on the opposite side, when we “receive” the signal, how do we know what to “read”? Is it the actual signal or our particular reflection on it? And if it is a personal perspective how do we form it? Is it affected from cultural, social or whatever else biased data?**

#Day 3: Magic Machine#

Today we are about to create something from nothing but adding to it a little bit of magic. We chose one need/desire and we try to address to its definition by an object which can do whatever we want it to.

\\\ Part1: “SOCIAL CONTACT”
Well the choice there was obvious, but what device can I create to address to that? I will try to analyse what social contact means to me and try rationalise it in order to get some inspiration. For me the most important part of any type of connection is the existence of two parts exchanging signals, ideas, opinions etc. It is a two way relationship which is build on common ground.
But what happens where there is only one part? For example what happens when people feel lonely and need help but they don’t know how or to whom they should address? and what happens when people want to help but they can’t reach the people in need? Here comes the magic machine! I call it “social sprinkles” and its structure and function is pretty simple. People that want to help, get it and spread “social sprinkles” around. People that need help use it to collect the sprinkles. Small detail. When someone gets sprinkles, his device is automatically spreading it around as well. In that way the “social magic” is created in a sense of evolving system.

PHOTO

Part2: “ACCEPTANCE”
For that term I will proceed in the same way. In my personal belief, acceptance can be distinguished in two parts; the self acceptance and the social acceptance. Of course these two are strictly related and one affects the other. Yet there is a fine line between what we see in ourselves and what others can see. *see* So it’s a matter of what we project or what others reflect? Every single person in your life is your mirror. *mirror* This means that others are reflecting parts of your own consciousness back to you, giving you an opportunity to really see yourself and ultimately to grow. Therefore, to change anything in your relationships, you need to be the change you want to see.
So let’s visualise this “mirror”; and not just any mirror.
https://www.youtube.com/watch?v=mqmIMvWnIV8  
Ok not that dramatic, but it is kind of a magic mirror. What if instead of plain glasses we could own “enchanted goggles” which can read our thoughts and can project our real magic? I picture them as a two layer filtered glasses, which on the layer closer to the eye have a “magic mirror” which can map the mind and trace the thoughts and feelings, and on the outer layer there is a projection screen showing, revealing your true self, without any insecurity or prejudice.

PHOTO2

Playing-making in that case worked perfectly as it helped us to give materiality or at least a visual context to values that seemed to vague and to understand how we can treat them as “objects” in order to be able to modify their “properties” ///
#Day 4: Planning the action#

It’s time; we have to deal with our idea face to face. And not only to make the context more clear and efficient but also build on it using the methodology from the previous days. We have to follow one of the 4 proposed strategies [living with our item, experiencing first person perspective, materializing an abstract idea, creating speculative artifact] and put it in our personal project.
As my interest is revolving around trust and in particular how we can use trust as a tool to bond communities, I decided to explore the area and find some key questions that should be answered in the first place in order to start building the project. One of the first things that concern me is if it is easier for someone to trust a person or confess something anonymously. To settle this issue an experiment -on field research- should be made. I should create a small “confession box” and hit the streets.
But first I should be sure that my idea i actually relevant to the common needs. For that reason I established a set of short questions, in which the answer is either yes or no. In that way I will be able to approach people and break the ice, and at the same time investigate public belief regarding my area of interest. The first, quite straight forward, question is if they think that trust is an important element in order to have social bonds. Then, if they feel that we trust enough and, finally, if they think that a space where we can express ourselves and connect is needed.
The plan was to conduct the experiment in 3 different stages; asking people that are close to me, friends and co-students at MDEF, then people inside IAAC to which I may be a familiar face but have no special connection and finally completely strangers, most preferably around PobleNou and Sagrada (different communities, ages, backgrounds etc). After the “opening questions” I would follow my instinct and try to establish a connection accordingly, in order to reach m final question; *“which is your biggest fear?”*
Apart from the fact that to my mind there is a close relation between fear and trust there is another issue I am interested in; to my personal belief if you feel at ease talking about your fears, thus trust, then it is easier to face them and raise your self acceptance, feel even more comfortable in your own skin and as a result with other and so on. But can you trust someone that you just met and confess your fear or is it easier to write it down, put it in a box and leave?
*I’ll find out soon enough, if everything goes according to plan*

#Day 5: Action and reflection#

I decided to start the experiment from the area around Sagrada Familia, where at any moment you can find people of all ages, nationalities, education backgrounds etc. People were more than willing to speak and help me with my little attempt to find some patterns and they happily answered the questions. The results were as expected. YES trust is important, NO we don’t trust enough, YES we need these kind of spots. Not everyone had the same approach but to these basic 3 more or less everyone agreed. So the difficult part now. Will they trust me with their fears? To my surprise, they did! I have to possible explanations. Either they did not want their fear written and there forever, or they actually made a conscious decision to trust me; or both. In any case this was not expected.

**So what now? How do I move on from here, if it is true that I have this “special power” and I can make people open up and speak? How can put this value in the project?
Maybe I should explore more how this thing works with me. Not wait if someone wants to come to me with their concerns, problems, thoughts, etc, but actually go there myself; provide a shoulder, an ear and an advice to anyone I can sense that needs it. Maybe by talking more with people I will be able to understand them and the “system” better.
Also I need to face some fears of my own in order to be even more engaged in that process. So I intend to go out of my comfort zone and exceed my personal limits, do things that I would not usually do, try new things, experience with all my senses.
Finally, I need to study the science behind the issue of trust; for example, what happens that exact moment that we decide to trust another person? I can experience it and understand the feeling but neuroscience could be useful. Maybe there is a pattern there, some common triggers, I don’t really know what I am looking for, but I am open to any discovery.
This is more than a project now; I think it just became personal. We’ll see**
