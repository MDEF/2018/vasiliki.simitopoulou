#03. DESIGN for the REAL DIGITAL WORLD

Week number 3 and finally there is something there that I can relate to. In the syllabus it was described as a two phased group seminar week, in which we have to “conduct a participatory design proposal to be presented and discussed” and then produce the best elements of each group’s proposals. The process is clear this time and the output almost defined (or at least I think so)

we get to modify and re-use all these amazing things we gathered from the streets of Poble Nou 2 weeks back -during Bootcamp week, in order to re-arrange the whole class 201, keeping in mind at the same time that this new creations should be able to be reused again and again when we are done with them.

 Fab Lab time, with all these new -for me- machines. I only know how to use laser cutter so it will definitely be a challenge to get to know how you design for the real world, how what you design transforms from an image to an object

So happy mode on!

For me up until that point the process of creating something was quite simple. There is this triangle of thinking, designing and discussing that works as closed as a closed loop until you reach to an output that is satisfactory enough and then move to production.

![](assets/w03_01.jpg)

And this is how this week actually started. With a brainstorm.
It was really interesting to see how we could recognise almost the same problems and same needs but express them in a different way.

*But how do we move on now? We have all these scatter ideas and put them into a (floor)plan.*

It’s a good thing we were writing-sketching our ideas on small post-it, as it was easier to group them and find a common ground.

![](assets/w03_02.png)

Words are now getting grouped, taking a form… with the help of Internet -for some inspiration- of course

                                                          ![](assets/03.gif)

So now that we have somehow an idea of what we need - what we desire, we need to put it in paper.. The designing process is not as easy as I expected when you have this multidisciplinary group. We may use almost the same words to describe objects but when it comes to putting them in an image everyone has a different perspective. From the selection of the tool used (software, pencils, etc) to the way the tool is used varies and the outcomes are uncommon.

*How does someone decide which tool should he use? Is it a matter of habit, convenience or stance?
Do we use the tools that express our ideas best or what we know most?*

![](assets/w03_04.png)

![](assets/w03_05.png)

 As mentioned above, the ultimate goal was to re-imagine our class, re-think the environment that is going to host us for the next few months. And for me that meant only one thing. No matter how the “new” furnishing will look we have to re-design how this whole space is divided and functions. In a way a solution should be found that the existing and new parts merge in a practical effective plan, taking into consideration the limitations of the given room.

![](assets/w03_06.jpg)

Thus, our first 2 days are almost over. We put together everything we have and -to me- it seems like an “exquisite cadaver” [1]. Sketches, ideas, intentions amassed in a few pages pdf. But presentation time is always the best part. You get to see what others do, try to explain out loud your points, get inspired, influenced, challenged.

![](assets/w03_07.gif)

After the presentation and discussion over each group’s propositions, here comes the jury time. And the verdict was loud and clear. We are going to design in detail and produce the mini fab lab; a moving device which will be our special MDEF working table providing pockets for all the tools and material we will need throughout the year.

Designing something that is going beyond a scale model is not an easy task. The structure should be clear and functional but appealing, the form should be simple but adaptable to our needs.

![](assets/w03_08.gif)
![](assets/w03_09.gif)

This takes me back to my pregraduate years..  Views, sections, details. An object designed, specified.

![](assets/w03_10.jpg)

![](assets/w03_11.jpg)

![](assets/w03_12.jpg)

20.10.2018 | The final object is not yet fully constructed. The creation of the appropriate files in order to use the fab lab equipment proved to be more difficult than expected. The assembling of the pieces failed and the result for now is disappointing.

![](assets/w03_13.jpg)


The project will be finalised and fully functional asap.
Nevertheless this experience taught me a lot. First of all, I should always remember that the key to accomplishing anything within a team is COMMUNICATION. We should try to explain ourselves better and listen to the others. I personally should show more PATIENCE when it comes to listening. People from different backgrounds may not be able to use the softwares or the machines but they have a different perspective and opinion which should be RESPECTed and VALUEd.

*Note to self: it’s the journey that counts and not the destination*























*
[1] a method by which a collection of words or images is collectively assembled. Each collaborator adds to a composition in sequence, either by following a rule or by being allowed to see only the end of what the previous person
*/
