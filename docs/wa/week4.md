#04. Exploring Hybrid Profiles in Design

Here we go… Fourth week just started. This week we are going to meet some people that pursued their dreams and passions against all odds and we will talk with them about alternative paths through the work field, challenges and opportunities arising and, of course, how to cope with all these, the innovative processes they follow, their motivations etc

3 days | 3 different workspaces | 3 different inputs

#Day1: Visit @ Domestic Data Dreamers | hosts: Pol Trias & Marta Handenawer | Carrer Pujades 77, 08005 Barcelona
Just across the street of the main building of IAAC there is this small paradise. Maybe not a paradise; more likely the house of Santa Claus’ elves , working hard to make magic happen. You only need to walk into their working area to feel the positive energy and become curious over their work. They call themselves a strategic communication consultancy design firm, like a cone between information and people, but they are much more than that actually. Their area of expertise is the conceptual design of experience; the design of the storytelling through data, with the purpose of dealing with the threats society faces.

Pol and Marta walked us through some of their projects and showed us how things worked.

1/ “Design Does” : a project by Elisava Design School and the Barcelona Design Museum which collectively explores how design tackles the challenges faced by society, at times offering improvements and, at others, doing just the opposite. Every design bares the weight of the responsibility and the impact on industry, people, social systems and cultural values. As modern era keeps changing, the role of designers alters in a way that -now more than ever, but probably less than tomorrow- he is evolving into a provider of solutions, a humanist, strategist and/or an agent of that change. During this exhibition, and by using technology as a medium of interaction with the public, a dialogue was created by collecting real-time data and displaying them. Visitors could share their sensations, reactions and opinions.

https://designdoes.es/

*As designers we always think a lot of our work but how can design actually affect our lives?*

2/ “I stand for” (by Beefeater) : an anthropological study about festival goers born between 80s and 90s, how they are, what they stand for. The research was combined with an on-field exploration; Bilbao BBK Live and Mad Cool 2017 festivals were the best field areas. There were set all sorts of interactive installations, from big double gates to people that requested your hi-5s. Loud music, nice atmosphere, high spirit, positive energy and lots of fun were the key points of this survey. And the results were fascinating as well. People participated massively and gave their best

https://www.behance.net/gallery/56811369/I-stand-for-Beefeater

*Could having fun become a strong tool in the designers’ hands?*

3/ “Making visible the invisible” : a project which took place during the General Assembly of the United Nations in September 2016, where the Heads of State and the Government from around the world discussed the role that data would play in measuring progress towards the future. The aim of the design was to make data about childhoods at risk visible. DDS worked with the Data, Research and Policy division from the UNICEF in order to transform the lack of data regarding the children at risk into a tangible experience, shedding some light on the unsustainable situation of many children in the world and encouraging very influential decision-makers to take action against this reality. The unique time machine that was designed enabled the audience to go back to their own childhood and connect with it, bringing meaning to the lack of data on serious issues and make them understand it, while explaining how important the access of information was for the policy making. The interactive installation was used by 246 politicians and had a direct impact on the agenda of the UNGA of the following year.

http://domesticstreamers.com/case-study/general-assembly-united-nations/

*How do you get to design an experience that will enable deeper understanding and empathy over serious issues that beset modern societies?*

After the small presentation of the DDS projects we got to participate in an interactive data collecting experience ourselves as we were asked to empty our bags, pockets, wallets and display our items in a way that would make sense to us; that would say our short story.
All kind of interesting things popped on the table.
For me it was another hard Monday (I am definitely NOT a morning person and Mondays are always the hardest to handle) and my bag was full of random objects. Truth is that almost every time my bag is like this but then I realised I actually have a routine before leaving the house. I go around asking things to myself and getting whatever fits the answer.

From this experience in the DDS workspace I ll keep these facts:
_to get the best results possible you should find a way to design things in an interactive, fun, person-oriented way
_a good design can have a major impact on people’s decisions, opinions, lives
_storytelling is a tool all designers should use to empower their projects; a good product without a good narrative is a useless product_


#Day 2: Visit @ Elisava | host: Ariel Guersenzvaig | La Rambla, 30-32, 08002 Barcelona
First time for us at Elisava School of Design. Well this has nothing in common with IAAC facilities. Elisava seems more like a “proper” school whereas IAAC is more like a hub-lab. I am so grateful we get both.. We can actually make the most of them both.

On this first visit we get to talk with Ariel, the Director of MUDIC and Co-Director of the Master in Design and Internet Web Project Direction; an interaction designer, who has defined products and services for big businesses, SMEs, NGOs, and state and government agencies, but also makes projects as an independent consultant in fields related to innovation focused on design and service design. His impressive resume reveals only part of his intriguing life and personality.

So...There he is standing in front of us explaining to us all this life through the past few decades. And by all I mean ALL; both professional and personal. The input of information was too much to handle. Starting at the mid 90’s and explaining how Internet was starting to take over (and how things were designed for that back then - things that now we take for granted) going through theoretical and practical issues, the evolution of designING -not design- and the web, and finally landing on AI technology and ethics.

He talked about subjects I already somehow know as I studied them for my thesis (and personal development), such as affordances and the human factor in designing, philosophy and ethics of technology, referring familiar to me names like Gibson, Heidegger, Moore, etc. And then he hopped to a whole new world for me, talking about economic factors. resources, skills, etc.

The cherry on top of this 3 hours session was talking about killer robots. There is a lot of information out there, but do we really understand what we read? Lethal autonomous weapons are almost here and we have to make a decision. Do we stand for or against them? Is there a solid point pro killing machines?
Trying to trace the path of arguments of both sides, Ariel mentioned the “jus bellum iustum”[1] and that reminded me of an article by Cambridge University Press I read a while ago, regarding the “unlawful war”.
*“Under contemporary international law, war crimes are conceived as particularly serious violations of the laws of armed conflict. Mere participation of rank-and-file soldiers in an unjust or unlawful war is generally not considered to warrant legal punishment. This position is based on the principle of equality between belligerents. During the last 20 years, this principle has been challenged by the so-called revisionist position in just war theory, as well as by certain scholars in international law. According to them, unjust or unlawful participants in armed conflict perpetrate serious wrongs. This article argues that their conduct is not only morally wrongful, but also that it should be criminalized under certain circumstances. On the basis of empirical research on cognitive biases, and on one of the leading accounts of legitimate authority in political philosophy, it argues that participation in war warrants criminalization only when the war is knowingly or manifestly unlawful. Furthermore, it claims that this position is not only sound at the level of deep moral principles, but that in fact it provides a persuasive reinterpretation of existing international law”.*
The strongest argument against the use of AI robots as soldiers is that the don’t comply with humanitarian law, therefore they are not able to make distinctions. What if the code says that a 5-years-old child is going to be a terrorist in 20 years; is it ok to kill it to prevent it?
Moreover in the case of killer robots, who is responsible if something goes wrong? If there is no malfunction or whatsoever, who is brought to court for the war crimes?
There are several campaigns against the use of killer robots and it would be really interesting to read their manifestos and understand their points of view
Eg: ICRAC, a Non Governmental Organization (NGO). We are an international committee of experts in robotics technology, artificial intelligence, robot ethics, international relations, international security, arms control, international humanitarian law, human rights law, and public campaigns, concerned about the pressing dangers that military robots pose to peace and international security and to civilians in war. https://www.icrac.net/

From this experience I kept this note:
_I need to study a lot now. It’s kind of scary how much we don’t know and may affect the way we design. We cannot afford to be ignorant.
My personal objective is to understand how things work first and then decide where and how I can intervene. So even if I don’t mind about economic modules and business models, I should be able to figure out how they are created, formed, functioning._

#Indicative reading list:#
-Information Architecture for the World Wide Web (P.Morville, L.Rosenfeld); the “polar bear book”
-The Design of Everyday Things (D.Norman)
-The Inmates Are Running the Asylum (A.Cooper)
-Creating Capabilities: The Human Development Approach (M.Nussbaum)


#Day 3: Visit @ Deubieta | host: Sara De Ubieta | Carrer Santander 42, 08020  Barcelona
Last day of our tour around Barcelona visiting unique workplaces. Save the best for last? For sure yes! In this pure industrial area with the storage houses and the big workshops, there was a small jewel waiting for us. Sara was there to welcome us with this positive energy you can't overlook. Her enthusiasm describing what she does and how, captivates you and makes you more and more interested -like shoe making is not exciting enough.

Leaving back (or maybe aside) her degree in Architecture, Sara decided to follow her passion about shoes and fashion and pursue her dream of researching, and found a way to combine both. She works on shoes through production and different ways of research; she researches new materials as an alternative to leather and new methods of making shoes without using patterns.

In a way she maintained the architectural background as she is still using terms such as materiality and form in order to define her projects, but at the same time she evolved into a craft-maestro who is more process oriented rather than outcome (product) driven.

Sara experiments a lot with materials that are far from conventional in fashion industry. Her process does not involve -in most cases- designing the pattern of the shoe, but working directly on the shoe lasts taking advantage of the properties each material has.

Her studio is full of prototypes either more conceptual or more wearable, all reflecting her never resting curiosity, her infinite imagination and her passion for what she does. Some of this vibe, she tried to impart to us by giving us materials, tools and only 1 hour to make an actual shoe. It is a hard task and especially if you have no idea how to use the tools or you are totally ignorant regarding the materials given; but the truth is that the experience was nice, educative and fun. The fashion line produced though is another story; we tried our best, we … oh well at least we tried!

http://www.deubieta.com/


#Day 4+5: REFLECTION DAYS

Back to Elisava, it was time to spend some time to digest the previous days’ experiences.
*So...what did we learn this week?*
All these people we met have sort of a common ground; they did not settle for what was already there for them but they reinvented themselves and created their own path. And their paradigms can teach us a lot.
First and foremost, we should always stay faithful to our dreams and try our best to fulfill our goals no matter the difficulties or whether other people support you or put obstacles to your way.
Furthermore, they made me feel that it is ok if along the way you decide to deviate from the path you had planned. Sometimes things emerge that there was no way you could predict and you should embrace the fact that your plan was not as consistent as you thought.
Additionally -and to quote the ancient greek philosopher Socrates- true knowledge exists in knowing that you know nothing [2]. You never get to have enough skills and knowledge and you should always be aware and prepared to get more inputs.
Last but not least, attitude makes success. Positive energy and self-assurance are the two keys that can help you unlock firmly closed doors and navigate you through the unknown path you select to take.

In order to find what suits you more you have to start from the beginning. Find people that are successful to what they do, compare yourself with them and find attraction and repulsive points. Then get some of these points and add them to your personal state and use them to reshape yourself.


Here it comes again. #how do you see your future self?#
I wonder if I will ever find the answer to that. Maybe if I do a list -as Oscar proposed- with things I carry from my past and ambitions, dreams, goals of the future, I can realise a certain path by forming the connections between these spots.

For me you should first know what you bring to the table; what are your assets and also your drawbacks. For example I know how to design both in a conceptual but also in a structural way; that’s what I have been taught and practicing over the past decades. I also have this strange ability to “read” people so I tend to add a more personal feature in what I do related to the audience each time. Throughout the years I explored the pool of interests I have and finally realised that the bottom line to all of them is the social aspect of everything. On the other hand, I have almost no experience on the workfield; my student, personal and professional project had always had a more theoretical approach and my comfort zone up until now was inside the academic sphere. This fact amplified my natural tend to procrastinate; when the project is not meant to be realised you can afford spending more time thinking about it, although this lead me getting deeper and deeper into matters -that maybe I should not- and then have to face the fact that every assignment has to end sooner or later and struggle to find my way out of the mess I created for myself. That, in addition to the impatience that characterises me, is a deadly combination when it comes to actual projects that need to be finished at a certain time with special specifications, etc;
When it comes to my future goals, things get more blurry. I know I want to help people make their lives more comfortable, easier, happier, better; *but what does that actually mean?* Do I want to run my own business/studio/whatever? Most probably no. Or at least I don’t think that it is a priority for me. I need to find a way to communities work together and evolve in a manner that -after a point- I won’t be needed anymore. Maybe this is it; an autonomous system for the communities, created alongside them. I strongly believe that the users should be involved in every step of the process and in a way that is essential to the designing and also effective for both them and the project. *So where is technology in that project line?* Maybe technology is just a tool to help the communication of ideas among the actors involved; or maybe it a fundamental part of the “product” itself. We cannot ignore the fact that technology is out there and progresses in a pace that we can hardly follow. But for me that is not scary; I mean we have to think it in a positive way and get advantage of everything that technological applications can offer. *what if instead of concerning about personal data privacy in social media, etc, we could actually use them to create a network that could offer help, comfort, company to anyone needed?*
We are able to do almost anything -or we will be soon enough- thanks to the technological advances. But #if we can do anything WHAT should we do?#

For me the bottomline of every action -personally- and every design -professionally- should be the social impact of our decisions. *But this is too vague.* Taking account the communities that we design for and actually ask them to become active actor throughout the process is one thing; ok I know I want to work with communities in co-design projects. *But in what way?* I want to make communities stronger, more aware of their true power; *But how?* They say a chain is only as strong as it weakest link. So I am thinking what makes a community a chain? For the best of my knowledge it is common background, common interests and trust. *trust; in a social context, trust has several connotations. The definitions may refer to a situation characterised by one party (trustor) is willing to rely on the actions of another party (trustee); the situation is directed to the future. In addition, the trustor (voluntarily or forcedly) abandons control over the actions performed by the trustee. As a consequence, the trustor is uncertain about the outcome of the other's actions; they can only develop and evaluate expectations. The uncertainty involves the risk of failure or harm to the trustor if the trustee will not behave as desired.  But how do we choose who to trust? And in general.. Is it easy to trust someone nowadays?* So what if we can work on trust with the communities, and design a system that enables that feeling of congeniality? Make people feel comfortable, talk about things that concern them and give or receive help from others. *now this seems like a vision*

I am looking forward to the next weeks, when this first blurry idea can start evolving, getting more inputs and forming into a project.
Expectations for the next few weeks:
Week 5: Understand modern era’s challenges. Maybe having philosophical debates over possible solutions, ethics, politics, etc, will help me understand better my intentions and my point of view regarding my personal project.

Week 6: AI !!! Get a glimpse over artificial intelligence and find some answers to my everlasting dilemmas regarding the use of technology in an extended way. *how, why, etc*

Week 7: Get to play with everyday objects and give them new properties. Maybe hacking an object will be more useful than trying to create a new from scratch.

Week 8: Experiment in order to clarify the idea and turn it into project

Week 9: Learn more about narration and storytelling and start working on the background that can support the project

Week 10: Find out how to predict future and use it to “teach” the system required for the project.

Week 11: Create the “tools” needed for the projects

Week 12: Define idea-project and plan the strategy

TO BE CONTINUED...













*[1] just war theory; a doctrine, also referred to as a tradition, of military ethics studied by military leaders, theologians, ethicists and policy makers. The purpose of the doctrine is to ensure war is morally justifiable through a series of criteria, all of which must be met for a war to be considered just. The criteria are split into two groups: "right to go to war" (jus ad bellum) and "right conduct in war" (jus in bello). The first concerns the morality of going to war, and the second the moral conduct within war. Recently there have been calls for the inclusion of a third category of just war theory—jus post bellum—dealing with the morality of post-war settlement and reconstruction. Just war theory postulates that war, while terrible, is not always the worst option. Important responsibilities, undesirable outcomes, or preventable atrocities may justify war. The historical aspect, or the "just war tradition", deals with the historical body of rules or agreements that have applied in various wars across the ages. The just war tradition also considers the writings of various philosophers and lawyers through history, and examines both their philosophical visions of war's ethical limits and whether their thoughts have contributed to the body of conventions that have evolved to guide war and warfare.

[2] “‘έν οἶδα ὅτι οὐδὲν οἶδα”; known greek proverb credited to the classical Greek (Athenian) philosopher Socrates, one of the founders of the Western philosophy and the first moral philosopher of the Western ethical tradition of thought. Even though he made no writings of his own, he is known chiefly through the accounts of classical writers writing after his lifetime, particularly his students. The conventional interpretation of the famous quote ascribed to Socrates, based on a statement in Plato’s Apology, refers to that Socrates’ wisdom was limited to an awareness of his own ignorance. Socrates considered virtuousness to require or consist of phronēsis, "thought, sense, judgement, practical wisdom, [and] prudence.". Therefore, he believed that wrongdoing and behaviour that was not virtuous resulted from ignorance, and that those who did wrong knew no better.*
