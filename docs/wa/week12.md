#12. Design Dialogues


So this is it! Last week of the first term and it’s time to create our MDEF expo; an exhibition with all our mid-final presentations of areas of intervention-project ideas. This is going to be interesting; and challenging; and fun.

*the biggest challenge for me remains. How can I visualise something as vague and theoretical as my area of interest? How do I present a project that is not clarified yet?*

I ll start from what I already have; the title can give me some first “images”, or more like posters. Maybe a small sentence like a motto can give some more information without revealing much though.

![](assets/confianca1.jpg)

Now, let’s go back to what happened all these past 11 weeks and read again the underlined notes; some critical questions or scatter thoughts can fill the puzzle or maybe create a cadavre exquis.

![](assets/TIMELINE.jpg)

And now it's time to open all the tabs I have been bookmarking all these past weeks that seemed related or interesting. There was this campaign entitled “tenemos que vernos mas” [https://tenemosquevernosmas.ruavieja.es/en/] which draw my attention to some really shocking statistics, regarding our future life. The team behind it try to show us in the most direct way that even though we all know that our relationships are the most important things in life, we spend less and less time with the people we really love. On the one hand, our modern lifestyle makes it hard to be aware of how we distribute our time. And on the other hand, human beings are simply not programmed to think about how long they have left to live, and that's why they spend life leaving everything for later, thinking that there will always be a moment to do all these things. By asking some simple questions (like the number of hours we spend together each time we meet, by the number of times we see each other over a year, where we live, etc) and cross-reference the information provided by each user, with the data form the National Institute of Statistics they provide a prediction based on our current behaviour. Then before giving you the final number, there are some general statistics regarding our future everyday life, in order to raise awareness over how much time we spend on activities that might seem meaningless and maybe help us reconsider how we want to organize our day-to-day and our priorities.
For me it was quite surprising to witness the outcome as I am expected to spend far less time with my beloved friends and family, than being online or sending messages. Then it occured to me; what if this fact is not the problem, but a medium to solve the problem? *What if we spend this time online -or at least some of it- trying to get together instead of isolating ourselves, or what if these messages could actually help someone instead of just chatting about hollow things…*
